---
layout: post
title: "Booking Restriction and datepicker"

postday: 2015/02/14
posttime: 11_50
author: Mandeep
---
As in the last [post](../../../../2015/02/14/working-booking/) I specified the booking was working but if we book the same hall for start and end time then it successfully added to the database as a booking, which should not be there. Hence to restrict them I found a solution of unique_together.

To use this we need to specify which fields we need to be unique together means we don't need any entry(booking) to be there with the same hall, start time, end time. Hence to accomplish this, I added in the models.py under the Booking model:

class Meta:

     unique_together = ('hall', 'start_time', 'end_time',)

Previously we have to input date in a character field, but now I used a widget.  Using this there are drop downs for selecting date, month and year.

In forms.py,

from .models import Booking

from django.contrib.admin import widgets

from django.forms.extras.widgets import SelectDateWidget

a = ('2015', '2016', '2017')

class BookingForm(forms.ModelForm):

    date = forms.DateField(widget=SelectDateWidget(years=a))

I imported the SelectDateWidget from Django widgets and then used it in the date field as shown in the last line above. There I passed a list 'a' as the years. This results into only 3 entries(2015,2016,2017) in the year choice field when we view the html(book.html).
