---
layout: post
title: "Recover corrupted .tar.gz on Ubuntu"

postday: 2014/12/27
posttime: 23_36
author: Mandeep
---
**STEP 1**: Install the gzrt tool by typing in the terminal:
    
    
    sudo apt-get install gzrt

**STEP 2**: Now move to the directory of corrupted file using command 'cd'

**STEP 3**: Execute command on corrupted file:>
    
    
    gzrecover copied.tar.gz

This command will create .recovered file in the current directory.

**STEP 4**: After recovery, extract the files:
    
    
    cpio -F copied.tar.recovered -i -v

Whether you want to recover .tar.gz file or .tgz file, the procedure will be same though you have to just rename the extention from .tgz to .tar.gz.

IThe amount of recoverable data may depend upon the cause of corruption.
