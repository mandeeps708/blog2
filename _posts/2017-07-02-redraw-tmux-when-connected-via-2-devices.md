---
layout: post
title: "Redraw Tmux when connected via 2 devices"

postday: 2017/07/02
posttime: 21_34
author: Mandeep
---
There can be a situation when you have to check your remote system from multiple devices. The situation could be: You have some heavy installation (long process) going on a remote server and you want a little break meanwhile. You may plan to see the progress while enjoying on the garden bench from smaller device like your smart phone. [Tmux](../../../../2015/07/24/tmux/) is a nice tool to work on remote systems. You may just simply connect to your remote server using some smartphone application like JuiceSSH and attach the tmux session already going on. So after a while, suppose you are back to your giant PC and connect to tmux again. You might notice that the screen will get shortened with a message at bottom right saying "from a smaller device". So there would be lot of extra space around your shell that you can't use. This situation can also be encountered when two people with different terminal window sizes work on a single tmux session. In this case, you may ask another person to change the window size.

But if another client is your phone as mentioned above or another similar situation, then you can deattach your smaller session, without touching, using:
    
    
    Ctrl-b + D
    
    
    
    

Note: it's captial D. Then it will ask you for the option to deattach from. Choose the smaller size and you are done.
