---
layout: post
title: "Change folder color in Ubuntu (Nautilus)"

postday: 2014/11/25
posttime: 23_18
author: Mandeep
---
![change folder color: Mandeep Singh]({{ site.url }}/attachments/folder.png?w=300)

*Change Folder Color*

Add PPA to system, open terminal (Ctrl+Alt+T) and type commands one by one:
    
    
    sudo add-apt-repository ppa:costales/folder-color
    sudo apt-get update
    sudo apt-get install folder-color

If you dont' want to add ppa then download deb file [Click here](http://ppa.launchpad.net/costales/folder-color/ubuntu/pool/main/f/folder-color/)

Restart Nautilus:
    
    
    nautilus -q
