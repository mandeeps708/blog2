---
layout: post
title: "Implemented View Bookings"

postday: 2015/02/27
posttime: 04_14
author: Mandeep
---
I had my sessional exams last week. I wouldn't talk much about them. :D

Okay now, as of the [first post](../../../../2015/02/01/booking-system/), where I showed the 3 modules: Click to book, view bookings and cancel bookings. As Click to book is working(not too much efficient yet) and I tried my hands at View Bookings page which means view function in views.py, view.html now needed to be updated. As of now, Raman completed the login part of the authentication (no logout :P ).

Hence the general idea is to fetch the values of the committed(final by admin) bookings from the database and load them into the html. To implement this, I used a table in the view.html and used a for loop to fetch all bookings one by one. You may see the file: https://github.com/rvirdiz/booking_system/blob/c6103ff49fdd55a2a03235a4de275b7ef058c151/static/templates/home/view.html

But using the for loop only will fetch all the values of booking from the database whether it is accepted by the admin or not. There is a status field in the Booking model which is hidden from the user and admin sets it to 1(true) if he accepts a booking.

So I filtered the results for the view.html as to show the bookings only whose status=1 (true) and this was done using:

boo = Booking.objects.filter(status =1)

In the views.py, under the view function add the above line to add a filter and then pass it to the context as:

return render(request, "home/view.html", {'boo': boo})

My experience is that it is a great feeling when you fetch values from a database. **_:)_**
