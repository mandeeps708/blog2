---
layout: post
title: "Installing mutt on Arch"

postday: 2015/12/25
posttime: 00_37
author: Mandeep
---
Install mutt by using the following command:
    
    
    sudo pacman -S mutt
    
    
    
    

Now create the base directories and a file using:
    
    
    mkdir -p ~/.mutt/cache/{bodies,headers}
    touch ~/.mutt/certificates
    
    
    
    

Use the following muttrc file, especially for **Gmail**:
    
    
    <https://github.com/mandeeps708/dotfiles/blob/master/.muttrc>
    
    
    
    

Modify it to according to your credentials and place it in the **home** folder (~/).

Now you can execute mutt simply using:
    
    
    mutt
    
    
    
    

For those who have enabled two-step verification in Gmail, directly entered password or written in muttrc will not work. You have to create an App password (<https://security.google.com/settings/security/apppasswords>) and enter the given password in your muttrc. Then it will work.

Although, it is not recommended to write your passwords in muttrc. You should set up gpg keys. For this refer to: https://wiki.archlinux.org/index.php/Mutt

**References**:

<https://wiki.archlinux.org/index.php/Mutt>

<http://www.vigasdeep.com/mutt-the-ultimate-mailing-client/>
