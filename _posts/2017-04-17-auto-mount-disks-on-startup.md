---
layout: post
title: "Auto-mount disks on Startup"
tags: linux, mount, startup
postday: 2017/04/17
posttime: 17_23
author: Mandeep
---
**sudo fdisk -l**

See which disk you want to auto-mount.

Suppose you attached a USB-drive and it's shown as "/dev/sdb1" in above command.

Its file-system will also be shown beside that. Match your type from System name and notice the corresponding Linux type.

 

System name

English name

Linux type

 

W95 FAT32

Microsoft FAT32

vfat

 

W95 FAT32 (LBA)

Microsoft FAT32

vfat

 

W95 FAT16 (LBA)

Microsoft FAT16

vfat

 

W95 Ext'd (LBA)

Microsoft extended partition

Not used

 

NTFS volume set

Microsoft NTFS

ntfs

 

NTFS volume set

Microsoft NTFS with read-write access

ntfs-3g

 

Apple_HFS

Apple HFS

hfsplus

Suppose it's W95 FAT32 (LBA) so your Linux type is vfat. If you are unsure about this type, you may consider mounting the device using: **sudo mount /dev/sdb1 **and then run **mount** command to see mounted file-systems and it's file-system type.

After gathering all the necessary information, open /etc/fstab with root permissions with your text editor e.g. **sudo vim /etc/fstab**

Then add the new entry,

**/dev/sdb1       /mnt/usb_drive       vfat       errors=remount-ro       0      0**

to fill in line with the following.

<**file system**>     <**mount point**>   <**type**>       <**options**>         <**dump**>  <**pass**>

Then save the file and exit. You may check on the next reboot if it is mounted.

**errors=remount-ro** option is for the case when some error occurs, the device will get remounted as read-only. The <**dump**> option specifies if the file-system needs to be dumped (back-up) for safe keeping. 0 here means it doesn't need to be dumped. And the <**pass**> means that the order which filesystem checks are to be performed at system reboot. The root filesystem should be labeled as **1**, while others as **2**. **0** if we don't want it to be checked. See **man fstab** command for more.
