---
layout: post
title: "Share Remote Terminal using 'GNU screen'"

postday: 2015/06/20
posttime: 12_15
author: Mandeep
---
GNU Screen is a utility to share terminal session with remote users.

##### Step 1: Installation
    
    
    sudo apt-get install screen
    
    
    
    

Then Set the screen binary (**/usr/bin/screen**) setuid root. Otherwise you'll get error regarding setuid root.
    
    
    sudo chmod +s /usr/bin/screen
    
    
    
    
    
    sudo chmod 755 /var/run/screen
    
    
    
    

 

#####  Step 2: Hosting

Now the host starts the session with:
    
    
    screen -S test
    
    
    
    

You may replace test with any name you want.

You have to enable multiuser by typing Ctrl+a then colon(:) then type:
    
    
    multiuser on
    
    
    
    

Now give permissions to the user you want to share the session with:

Type Ctrl+a then colon(:) and type:
    
    
    acladd username
    
    
    
    

replace username with the remote user's name.

 

#####  Step 3: Remote access

Now it's remote user's turn to come into play. He have to ssh into the host computer by:
    
    
    ssh username@host_address
    
    
    
    

replace username with the user's account name on host computer and host_address is the address of host computer.

Now to connect to host's terminal screen:
    
    
    screen -x host/test
    
    
    
    

host is the name of the host user.

Now the remote user can see what the host user types and he can also edit that.

If remote user wants to detach the session then he can do so by:

Ctrl+a and press d.

 

###### Source:

List of commands: http://aperiodic.net/screen/commands:start

http://ubuntuforums.org/showthread.php?t=299286
