---
layout: post
title: "Gitignore"

postday: 2015/09/29
posttime: 23_38
author: Mandeep
---
You might have noticed while creating a repository on GitHub, it asks if you want to add a license and a .gitignore file. So, what is a .gitignore file. It is basically a file that you create in your git repository to ignore some files and/or directories while committing.

If you make some changes to source some files and testing them by executing, then it's more likely that you'll have some additional files that you don't want to be on GitHub. So you will simply put the files into a .gitignore file and  that's it. Rest it'll take care next time before committing.

If you don't have a .gitignore file yet in your GitHub repository then you can just tell GitHub to create one for you. Just click the '+' button that you use to create a new file in that repository. Then in the name input box, type .gitignore and then a line will appear to tell you if you want a .gitignore file.

![adding gitignore]({{ site.url }}/attachments/gitignore.png)Click on None and you'll get a drop-down list with a search box. Enter the name of the language that you are working on. e.g. Python. After that, you need to just commit the file.

Now if you want to ignore some files with a particular extension, just add it to .gitignore.

**For example**: If you want to ignore all .pyc files within your repository, then you may add *.pyc in your .gitignore file.

You can also add a whole directory to the ignore list. e.g. You have a MAIN directory and within that directory you have folders like bin and you don't know the number and/or location exactly. Then you may simply add the following the .gitignore file:
    
    
    MAIN/**/bin/
    
    
    
    

This will ignore all bin folders within the MAIN folder recursively.

If you want your changes to take effect, you may need to execute the following command in the terminal:
    
    
    git rm --cached -r .
    
    
    
    

It will just reload your git repository for the changes to take effect. It will remove all ignored files.

You can see a list of .gitignore files at: <https://github.com/github/gitignore>
