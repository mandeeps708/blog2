---
layout: post
title: "Editing Open Street Maps"

postday: 2015/11/27
posttime: 11_17
author: Mandeep
---
I just signed up on OSM website i.e. [www.openstreetmap.org](http://www.openstreetmap.org). After signing up, I explored it a bit and located my house on it. It was quite empty there (nearby). Now we can add places, roads, streets etc. using three functions: Point, Line and Area. So I selected Area of my home and assigned it my name. You may search with my name; it's the only one entry that's appearing (as of now). Then I edited the map of my college GNDEC a bit. Also added my old school Guru Nanak Khalsa Sen. Sec. School. Earlier, I couldn't find even any mark related to it. Then added more neighbourhood houses to it. Now it's quite colourful over there. :)

One thing I noticed on the osm website is that we can't see satellite view normally. But while editing maps, we can see the imagery (satellite view) using bing or mapnik. Please correct me if something wrong with it.
