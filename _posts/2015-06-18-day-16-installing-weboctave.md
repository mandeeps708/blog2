---
layout: post
title: "Day 16 - Installing WebOctave"

postday: 2015/06/18
posttime: 11_40
author: Mandeep
---
Today I tried my hands on Weboctave. Weboctave is a package that provides functionality to run Octave on a web server. It provides support for same functionality as we run Octave in terminal. It provides a text input box on a web page in which we can type our octave commands. There is a 'Submit to octave' button pressing which the command executes and the output is returned below the input box. It also supports plotting graphs as it computes on server and return the image of that output graph on HTML page.

So, I downloaded the weboctave package from the website: http://knn.mimuw.edu.pl/weboctave-project/

Website of Web Interface to Octave http://weboctave.sourceforge.net

Then extract the weboctave-0.1.0.tar.bz2 and it contains the README file which says the basic requirements:

_1) WWW server with PHP>=5 support (ex. Apache)_

_ 2) SQL server (ex. MySQL, may be remote)_

_ 3) Octave>=3.0.0_

INSTALLATION PROCEDURE: 1) Unpack webctave archive to a directory available to be accessed by WWW server.

In my case I extracted it in my web document root i.e. /var/www/html/ 1a) Depend on your permission policy, you may have to change premissions, in order to files be accessible by WWW server. Particularly, you have to change permissions to directories "data" and "Logs" to be writeable by WWW server.
    
    
    sudo chown www-data:www-data data/ -R
    sudo chown www-data:www-data Logs/ -R
    
    
    
    

2) Create database to be used by WebOctave.

I created a database called "weboctave" from MySQL command line as:
    
    
    create database weboctave;

Then grant permissions according to the MySQL user may be something like this:
    
    
    GRANT ALL ON weboctave TO 'www-data'@'localhost';

3) Set database entries. Use "setup/db.sql" script, but edit it first to supply the database name.

In the setup folder under the weboctave folder, there is a file named 'db.sql'. Open it using any text editor and replace database in line 18 (it may differ) with your database name. In this case, I used:
    
    
    USE weboctave;

You must be in the setup folder and open MySQL console shell using something like:
    
    
    mysql -u root -p

change username (from root) accordingly and enter password.

Now execute this command:
    
    
    \. db.sql

4) Copy "config/config.php.example" to "config/config.php".

After traversing to config directory, you may copy file as:
    
    
    cp config.php.example config.php

Believe me it helps in reverting to default version if you modified the file incorrectly later on.

5) Edit "config/config.php" and set your preferences. You have to set database data.

Open the file config.php with any browser you like:
    
    
    vim config.php
    
    
    
    

On line 28, 31, 34, 37 there are some details about the server that needed to be changed:
    
    
    define("DB_HOST", "localhost");
    
    
    
    
    
    define("DB_USER","root");
    
    
    
    
    
    define("DB_PASSWORD","1234");
    
    
    
    
    
    define("DB_DATABASE","weboctave");
    
    
    
    

As it is clear from the comments that what these fields are all about.

Now your WebOctave environment is ready to work and should be accessible by any web browser.

Go to the web browser and open localhost/weboctave

Here weboctave is the name of folder that's placed in the web document root.

If you face any problem showing the octave page then change the permissions of weboctave folder recursively.

But after installation I am not able to run the commands through weboctave. An error similar to this one is displayed:
    
    
    'dispatch' undefined
    called from
    at line 99 column 1
    
    
    
    
