---
layout: post
title: "Import FreeCAD in Python"

postday: 2016/07/23
posttime: 04_52
author: Mandeep
---
If you've compiled FreeCAD, but don't know how to import it as a package in Python then read on.

First of all, you need to locate where the FreeCAD.so file is.

> locate FreeCAD.so

Mine is in _/home/mandeep/Desktop/build/lib_.

To be able to do:

> import FreeCAD

You'll have to go to that directory and run python and if you now try to import, it will work. But most probably, you won't agree to use it as it might feel impractical.

Another method is to append this path to sys.path. For this, you need to edit a file site.py.

For example,

> sys.path.append("/home/mandeep/Desktop/build/lib")

The site.py would probably be where your python lib directory is. It may probably look like: /usr/lib/python2.7. But I am using the Python within the virtualenv so the python lib is within the virtualenv e.g. venv/lib/python2.7. Here venv is my virtualenv.

Read more about this at <https://docs.python.org/2/library/site.html>

The idea to edit the site.py file is that it gets initialized with Python. So you can now import your packages from anywhere.

**Source**: <http://www.freecadweb.org/wiki/index.php?title=Embedding_FreeCAD>

**Reference**: <http://stackoverflow.com/a/15109881/3784226>
