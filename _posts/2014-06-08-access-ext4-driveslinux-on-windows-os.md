---
layout: post
title: "Access Ext4 drives(Linux) on Windows OS"

postday: 2014/06/08
posttime: 09_57
author: Mandeep
---
> **# Access Ext4 drives of Ubuntu on Windows**

Many of us has dual-booted our systems like Ubuntu (linux) with Windows. In Ubuntu, the drives have the format of ext4 usually for file system. But we can not access that drive in Windows. So here is a tool called Ext2Fsd.

Ext2Fsd is an ext2 file system driver for Windows  (2k,  XP, Vista and Win7). It’s a free software. Anyone can modify or distribute it under GPL2. I have tested it on Windows 8 too. But it mounts the ext4 drive with read-only permissions. So we can not write or delete anything to/from the disk. But we can open, copy and execute files. But that's better than to can't even see the drives. Here is the link for downloading page where you can select your package (.exe, .zip, .7z)  [Download Page ](http://sourceforge.net/projects/ext2fsd/files/Ext2fsd/0.52/). It also works for ext3 formats.

I had downloaded the .zip file and extracted and simply installed it like any other software in Windows. But to show the ext4 drive on each time Windows starts then we have to once run this software or you can find a way to automatically run at startup.

 
