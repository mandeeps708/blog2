---
layout: post
title: "Installing git-cola (git GUI client) on ArchBang"

postday: 2015/12/31
posttime: 04_15
author: Mandeep
---
[git-cola](https://github.com/git-cola/git-cola) is a git client having a GUI. Visit https://github.com/git-cola/git-cola to install for a different distribution or for further detail. There are many other GUI clients. You may see them on <https://git-scm.com/download/gui/linux>

Now it requires sphinx (for documentation) to be installed. So install it using:
    
    
    sudo pip install Sphinx==1.3.3
    
    
    
    

It depends on python2-pyqt4 package. So install it using:
    
    
    sudo pacman -S python2-pyqt4
    
    
    
    

Install tk (if not already installed):
    
    
    sudo pacman -S tk
    
    
    
    

_**Note:** If you don't install _tk_ and run git-cola it'll give error like:_

_/_usr_/bin/_gitk_: line 3: exec: wish: not found_

Now install git-cola using yaourt:
    
    
    yaourt -S git-cola
    
    
    
    

 

There are two commands: **git-cola** and **git-dag.**

**git-cola** includes all the workflow like showing currently unstaged files (on right) and after clicking one of them, it shows in the left large pane. Also it uses gitk to visualize the branches.

**git-dag** includes the visual representation of the git log command and shows the diagram of the commits and all that.
