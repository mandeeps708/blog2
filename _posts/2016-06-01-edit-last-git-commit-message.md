---
layout: post
title: "Edit last git commit message"

postday: 2016/06/01
posttime: 04_41
author: Mandeep
---
If you have accidently entered wrong commit message or want to add something more to it?

Here is the command:
    
    
    git commit --amend

It'll open a text editor window and lets you edit the commit message in the first line. Then save and exit the text editor and you're done.
