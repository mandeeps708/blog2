---
layout: post
title: "Repairing Grub of Ubuntu 14.04 (Without Internet)"

postday: 2015/01/28
posttime: 22_29
author: Mandeep
---
If you are having some problems on booting Ubuntu(or having grub error on boot) or you don't have access to already installed Ubuntu then read on.

You need to create Ubuntu Bootable USB, you can follow the tutorial here: <../../../../2015/01/28/make-bootable-usb-ubuntu-using-different-os/>

Once you are ready with your bootable USB, set the Boot Order Priority to USB. Then boot through it(by restarting) and Select Try Ubuntu option.

Then it will take few seconds then it will boot up.

**STEP 1:** You need to check on which drive Ubuntu was installed. You can check it by various methods. Open Dash(Start button of keyboard) and search for "Disks" and press "Enter". This will open Disks software. Now you need to identify on which you installed Ubuntu. You can check it by identifying the size you allocated to it and the file system. The file-system will surely be EXT4. Once you know which is your Ubuntu partition, then click on it and you will see information about it like its size and device. Note down the Device number: it may look like /dev/sdaX where X may be replaced by any number like /dev/sda2 etc.

**STEP 2:** Open Terminal(Ctrl+Alt+T).
    
    
    sudo mount /dev/sdaX /mnt

Here replace X by the device number you noted in the previous step(/dev/sda2 etc.).

**STEP 3:** Now you need to bind the directories to update changes to old Ubuntu, copy paste these lines in terminal:
    
    
    sudo mount --bind /dev /mnt/dev &&
    sudo mount --bind /dev/pts /mnt/dev/pts &&
    sudo mount --bind /proc /mnt/proc &&
    sudo mount --bind /sys /mnt/sys

You can use Ctrl+Shift+v to paste into terminal (or just right click and paste).

**STEP 4:** Now jump into chroot.
    
    
    sudo chroot /mnt

**STEP 5:** Now execute these commands ony by one:
    
    
    grub-install /dev/sda
    
    
    grub-install --recheck /dev/sda
    
    
    update-grub

**STEP 6:** Now you are done, just last step is to unmount everything and exit:
    
    
    exit &&
    sudo umount /mnt/sys &&
    sudo umount /mnt/proc &&
    sudo umount /mnt/dev/pts &&
    sudo umount /mnt/dev &&
    sudo umount /mnt
    
    
    
    

Now restart your system and remove USB before system boots up and you will most probably see the GRUB screen. :)
