---
layout: post
title: "Gunicorn with Django"

postday: 2015/10/12
posttime: 11_06
author: Mandeep
---
While searching for deploying the Django app, I found something called Gunicorn. I tried:
    
    
    gunicorn sage.wsgi
    
    
    
    

where sage is the name of the Django project. And now the django app can be accessed from the default address.

To make it automatic, I had to do some tweaks like creating file in /etc/init and creating its symlink in /etc/init.d/ etc.

Here is the file civiloctave.conf in the folder /etc/init :
    
    
    description "civiloctave"
    start on runlevel [2345]
    stop on runlevel [06]
    respawn
    respawn limit 10 5
    
    
    
    
    
    script
    NAME=sage
    PORT=8000
    NUM_WORKERS=2
    TIMEOUT=120
    USER=mandeep
    GROUP=www-data
    LOGFILE=/var/log/gunicorn/$NAME.log
    LOGDIR=$(dirname $LOGFILE)
    test -d $LOGDIR || mkdir -p $LOGDIR
    cd /home/mandeep/CivilOctave/$NAME
    gunicorn sage.wsgi
    
    
    
    
    
    exec /home/mandeep/CivilOctave/$NAME/manage.py \
    -w $NUM_WORKERS -t $TIMEOUT \
    --user=$USER --group=$GROUP --log-level=debug \
    --name=$NAME -b 127.0.0.1:$PORT \
    --log-file=$LOGFILE 2>>$LOGFILE
    end script
    
    
    
    

 

After creating its symlink in /etc/init.d/ and rebooting, the process starts itself and then we can open the address (i.e. localhost:8000) anytime in the browser to see it working. I couldn't get it working through apache. I'll get on that later on.
