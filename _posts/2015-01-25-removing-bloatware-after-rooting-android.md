---
layout: post
title: "Removing Bloatware after Rooting Android"

postday: 2015/01/25
posttime: 01_34
author: Mandeep
---
Have you rooted your phone just for having some memory free by uninstalling unnecessary applications provided by the stock ROM(default Android)? For more about rooting and ROMs visit the [Android](../../../../category/android/)  category. So how to do that. There are several methods to do this and I am explaining one of them here now. So I am using an application named "Smart Booster". **STEP 1:** You can download it from here: _**[Download Link](https://drive.google.com/#folders/0B7A1mjqF9sKsc0k1QUtMb19qSlk)**_ or ([Alternative Link](https://mega.co.nz/#!yJdlQSaK!TBtQHTM35QdVtONBX5Eq1FCL0y_1X33_BcFCQIpTbkc)) Now after downloading it, transfer this .apk file to your Android phone via data cable or bluetooth(if not already). **STEP 2:** Now locate the transferred .apk file on your mobile via the File Manager. Click on it and Install it. After installing, Open the app. **STEP 3:** It will ask for superuser access(Because for removing system application, you need to have root privileges. _Allow this application._ Now it will open the following screen:

![Smart Booster first screen]({{ site.url }}/attachments/openbooster.png?w=180)

*First screen*

**STEP 4:** Now click on "_App manager_" 3rd icon in the bottom list. Now you will see a screen similar to this one:

![Smart Booster options]({{ site.url }}/attachments/backup.png?w=180)

*Smart Booster options*

**STEP 5:** Now click on first option "_Backup & Uninstall_". The next screen will look like following showing the User installed applications' list:

![Smart Booster User application list]({{ site.url }}/attachments/applist.png?w=180)

*User application list*

**STEP 6:** Now _slide left_ and access the System application part on right side of this app. It will show you the list of system's pre-installed applications:

![System Application list]({{ site.url }}/attachments/dowhatuwant.png?w=180)

*System Application list*

**STEP 7:** Now it's up to you to decide what to remove(according to your need), but be sure before un-installing any system application. You may click on any application in the list to have some options. There will be 3 options: Backup, Uninstall and Disable. If you are not sure about any application then it is recommended that you first take backup of that application.  Then do uninstall. You will see a small "message toast" to tell you if it's successful or not. If something like this appears: "App not deleted" then try to Uninstall again. It will show "App Deleted" message on screen. Then go the Home Drawer screen and see for the application icon if it is removed or not. Thanks for reading.
