---
layout: post
title: "Hatching with images !!"

postday: 2014/07/08
posttime: 23_18
author: Mandeep
---
As the task assigned to me was about hatching. I was searching  about it on Internet from the last week. But it didn't help so much. I came to know about various libraries like OpenGL and Cairo etc. during this session. As [we](http://gurpindersingh751.wordpress.com/) were preferred to use Cairo to hatch an object, we tried with this. And we tried with image patterns to be filled within the objects. After some struggle, we got some results.The code is : <https://github.com/mandeeps708/hatching/blob/master/img.c>. By this time I learned to use [github](www.github.com) a little. My github link is : [github.com/mandeeps708 ](github.com/mandeeps708)

We used an image like   ![new](http://mandeep7.files.wordpress.com/2014/07/new.png) this one. And just put it into the program as the object background. But I think, we should do something extra.

There was another Latex presentation today [Amitt](http://amittbhardwj.wordpress.com) and [Ashu](http://ashusingh11.wordpress.com). It was about writing math equations and diagrams like we see in the Mathematics Books. Latex is the software that can do it easily. We just have to write appropriate code and that's it. It shows the vastness of the software than Microsoft products. Because obtaining results like these are near to impossible if we compare the efforts. It was good presentation.
