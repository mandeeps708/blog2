---
layout: post
title: "LATEX Presentation"

postday: 2014/07/02
posttime: 11_56
author: Mandeep
---
It was a rainy morning, so I had to wait for the rain to stop. So I reached TCC at 10 am. Today there was a presentation on LaTeX by [Arun](http://aarunbishnoi.wordpress.com/) and [Gunpreet](http://gunpreetahuja.wordpress.com/). LaTeX is a document preparation system for high quality typesetting. Basically, it can be used for book publishing, presentations, publication of scientific documents etc. LaTeX is based on the concept of defining the template first for entire document as in case of HTML we use CSS to design it. It has many features like:

  * When we are finished with our work it creates the 'table of contents' page itself by specifying the page number on which we want it.
  * Title page is created easily.
  * Produces professional quality results.
  * Just add a template and specify images and it will choose the location where it will appear best.

and many more.

LaTeX can be installed in Ubuntu by typing following command into the terminal :
    
    
    sudo apt-get install texlive-full

The package is quite large in size i.e. about 1.6 GB. I am still downloading it. I tried to copy the .deb package files from Arun but it copies only 500 mb of files that are useless because when I installed some of them then Ubuntu showed errors on the panel like "broken packages". Then I removed all broken packages using Synaptic Package Manager.

LaTeX documents have the extensions .tex which can be converted into pdf using terminal by this command :
    
    
    pdflatex filename.tex

We have to include packages for using different objects in our document like images, code etc. For example, we can use
    
    
    \usepackage{graphicx}

and then in the document section we include :
    
    
    \includegraphics[width=2cm, height=2cm] {image.png}

It was nice presentation.

 
