---
layout: post
title: "IRC: Nickname is already in use"

postday: 2016/04/27
posttime: 23_59
author: Mandeep
---
IRC is Internet Relay Chat. And it is a text-based protocol used for communication. It follows client/server architecture. There is a channel hosted somewhere on a server and you use your client application to connect to that server (channel).

I have used **XChat IRC** client and **weechat** (console based, for 2-3 times).

So coming back to the title of this post. Ah! You are probably here due to that only.

I had XChat set up on one operating system (let's say Arch) and a nick registered e.g. **_mandeep_7_**. Now suppose, due to some reason, when I temporarily shifted to another operating system (let's say Ubuntu). So if now I want to use the same Nick (**/nick mandeep_7**)** **then it will give the error: Nickname is already in use. There may be several reasons like sudden shutdown and you still are logged in to the particular server.

So what I tried is:

**/`ghost mandeep_7 `password**

Use your password instead of "password". Now try the command **/nick mandeep_7**

You'll now be able to identify yourself as old user (mandeep_7) on the new OS.

You should now try:  **/msg NickServ identify <password>**

Another thing to look for is linking.

**Source**: <http://archive.oreilly.com/pub/h/1940>
