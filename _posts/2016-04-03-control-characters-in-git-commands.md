---
layout: post
title: "Control characters in git commands"

postday: 2016/04/03
posttime: 18_16
author: Mandeep
---
While working on a git-enabled project, it may happen sometimes that you modify a file and then see the changes using git diff or any other command then it shows control characters like "**^M**". This may seem annoying.

You can hide it using:
    
    
    git config core.whitespace cr-at-eol
    
    
    
    

On the successful completion of the above, commands like _git diff_ and _git show_ will stop showing these characters. But it will do on a per-project basis. If you want to do it globally, do:
    
    
    git config --global core.whitespace cr-at-eol

 
