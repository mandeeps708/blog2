---
layout: post
title: "GitHub API"

postday: 2016/06/11
posttime: 23_40
author: Mandeep
---
As I fetched macro code using [BeautifulSoup](../../../../2016/06/06/beautifulsoup/). Now the FreeCAD workbenches are hosted at one place i.e. <https://github.com/FreeCAD/FreeCAD-addons/>. I needed to fetch the file details from this repository from GitHub. It was already done in Python by parsing the HTML and scraping it. I tried a Python module called BeautifulSoup that makes it easy to play with the HTML within Python.

But these aren't the permanent solutions. We'll have to make frequent changes in our code if the source website (GitHub) changes. So I thought of using GitHub API. It'll be more of a stable solution.

Here is the list of libraries you can use in any programming language: <https://developer.github.com/libraries>. I am going to explore PyGithub.

**Install PyGithub**:

> _pip install pygithub_

For more, read: <http://pygithub.github.io/PyGithub/v1/introduction.html>

Stay tuned!
