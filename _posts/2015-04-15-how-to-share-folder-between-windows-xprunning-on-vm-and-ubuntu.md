---
layout: post
title: "How to share folder between Windows XP(running on VM) and Ubuntu"

postday: 2015/04/15
posttime: 23_54
author: Mandeep
---
After having all things set up i.e. Installing VirtualBox, Installing OS on VirtualBox, you might have wondering how to share something between Ubuntu(Primary OS) and XP(on VM). For this we need to install Guest Additions image. For this, after running XP in VM, go to Devices menu on the top and select 'Insert Guest Additions CD image' and it will ask you for downloading it.

![v1]({{ site.url }}/attachments/v1.png?w=300) So download it (size around 65 MB) and reboot if it don't appear in the My Computer yet. Now it will appear in the My Computer. Then double click on that and install it.

![v6]({{ site.url }}/attachments/v6.png?w=300)

![v7]({{ site.url }}/attachments/v7.png?w=300)

Now click on Settings > Shared Folders > Machine Folders and select a folder(may create a new one like below).

![v5]({{ site.url }}/attachments/v5.png?w=300)

![v2]({{ site.url }}/attachments/v2.png?w=300)

Then tick on 'Auto-mount' and 'Make Permanent' if you want to mount it everytime you start the VirtualBox.

![v3]({{ site.url }}/attachments/v3.png?w=300)Click OK. You'll find a drive like icon that will point to the 'shared' folder in Ubuntu.

![v8]({{ site.url }}/attachments/v8.png?w=300)You can check the sharing by creating a new file in that folder and accessing that from Ubuntu..

![v9]({{ site.url }}/attachments/v9.png?w=300)![v10]({{ site.url }}/attachments/v10.png?w=300)

 

 

 

 
