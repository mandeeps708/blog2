---
layout: post
title: "4th July - getShape modified"

postday: 2015/07/04
posttime: 23_57
author: Mandeep
---
I modified some limits of circularity that is used to recognize the shape. Also now when calling the function, it displays the image with the shapes highlighted with a bounding box.

The original image (importme.jpg) is:

![Robotic Vision Worksheet](https://github.com/mandeeps708/RoboticVision/blob/master/importme.jpg?raw=true) So the whole process is:
    
    
    im = iread('importme.jpg', 'double');
    im2 = igamm(im, 'sRGB');
    
    
    
    
    
    r = getColor(im2, 'red');
    Do you want to set threshold value?[y/n]n
    OK entering applying default threshold...
    thresholdRed =
    0.4500
    thresholdGreen =
    0.4500
    thresholdBlue =
    0.4500
    
    
    
    
    
    rc=getShape(r,'circle');
    
    
    
    

![getShape output](http://snag.gy/4q2nH.jpg)

The code for the functions can be seen at: https://github.com/mandeeps708/RoboticVision
