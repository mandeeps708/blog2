---
layout: post
title: "Basic Template and Views"

postday: 2015/02/09
posttime: 07_35
author: Mandeep
---
Now we had our models ready and we need to add we could only add new hall and booking from the default 'admin panel' that comes with Django. But if we want that to be done on the user side then we must use html pages. So in Django, we use a proper way to show html pages. The system is called Template system. Templates are generally html files.

Django is based on DRY principle which means Don't Repeat Yourself. In the template system django follows the same. If we create a base html that can be used as a base for all other html pages, and we can use {% raw %} **{% extends "base.html" %}** {% endraw %} in other html files. And we use {% raw %} **{% block content %}** {% endraw %} {% raw %} **{% endblock %}** {% endraw %} in the base.html for the variable parts of the pages. In other pages we just use {% raw %} **{% extends "base.html %}** {% endraw %} {% raw %} **{% block content %}** {% endraw %}<h3>Bookings</h3> {% raw %} **{% endblock %}** {% endraw %} Hence we don't have to repeat the base.html again and we have to write only one new line <h3>Bookings</h3> for a completely new page. The major advantage is that we don't have to use all the necessary html tags like <html>, <head>, <body> and their closing tags again and again into all the template files. For this we just extend the base page and use all the tags defined in it. I tried my hands on bootstrap and used some of its classes, but it is not properly implemented yet.

Hence till now there is a views.py is created in which 4 functions(home, view, book, cancel) are defined that call the templates (html) to be rendered when a particular URL(urls.py) is visited in the browser. urls.py is a file for defining the regular expressions that can be used to call the views that further call the html to be rendered. Quite confusing!!!
