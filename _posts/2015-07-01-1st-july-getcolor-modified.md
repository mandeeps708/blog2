---
layout: post
title: "1st July - getColor modified"

postday: 2015/07/01
posttime: 23_58
author: Mandeep
---
Today I tried to modify the function of getColor that I made some days ago. Now I added morphological operations into it.

Here is the getColor.m function. I reduced the number of arguments from 3 to 2 and set the value to 0.45.
    
    
    function out = getColor(image,  color)
      Y = image(:,:,1)+image(:,:,2)+image(:,:,3);
      redChroma = image(:,:,1) ./ Y;
      greenChroma = image(:,:,2) ./Y;
      blueChroma = image(:,:,3) ./Y;
      threshold = 0.45;
    
    
    
    
    
      redBinary = redChroma > threshold;
      greenBinary = greenChroma > threshold;
      blueBinary = blueChroma > threshold;
    
    
    
    
    
      switch(color)
        case 'red'
          out = manmorph(redBinary);
    
    
    
    
    
        case 'blue'
          out = manmorph(blueBinary);
    
    
    
    
    
        case 'green'
          out = manmorph(greenBinary);
    
    
    
    
    
        otherwise
          fprintf('Invalid color option. Use red, blue or green in single quotes only.');
    end
    
    
    
    

 

Here is my function manmorph.m, that has been used above:
    
    
    function out = manmorph(image)
      ip = iopen(image, ones(10,10));
      out = iclose(ip, ones(10,10));
    
    
    
    

Actually iopen and iclose functions processes erosion then dilation and dilation then erosion respectively. But I need to use only iopen or iclose function because I need only one process (erosion then dilation) but the blue circles were getting some white dots (unwanted blobs) so it worked by chance.

Today, also listened to the presentation by [Gaganjyot](https://github.com/gaganjyot) on emacs (a text-editor). He explained about the features of emacs and how he uses his own key-bindings to make it more useful.
