---
layout: post
title: "Inducting Librehatti"

postday: 2015/06/25
posttime: 11_40
author: Mandeep
---
LibreHatti is basically an E-Commerce cum [CRM](https://en.wikipedia.org/wiki/Customer_relationship_management) (Customer relationship management) [Django](../../../../2014/10/29/installing-django-1-7-on-ubuntu-14-04/) Web Application. Today I installed [LibreHatti](https://github.com/GreatDevelopers/LibreHatti) on my localhost.

**Installation guide** can be followed at <http://greatdevelopers.github.io/LibreHatti/>

**Code on Github**: <https://github.com/GreatDevelopers/LibreHatti>

I set up [virtual env](../../../../2014/11/17/working-with-django-in-virtual-environment/) and installed everything specified in requirements.txt file.

I faced problem related to static directories and database name. In both the cases, the problem was of spellings of LibreHatti in the path in settings.py file. So replacing 'h' with 'H' solved the issues. The following is the screenshot of main interface of LibreHatti.

![librehatti]({{ site.url }}/attachments/lh.png)

 I had some issues during installation and then I was told to use dirty branch instead of master. So I cloned again from github because first I used --depth 1 flag while cloning so could not checkout to the 'dirty' branch. Actually master branch also had an issue related to some form. So changing to 'dirty' branch solved the problem.
