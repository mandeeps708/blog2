---
layout: post
title: "Installing Fedora 20 alongside Ubuntu and Windows 7 (Triple Boot)"

postday: 2014/11/23
posttime: 00_20
author: Mandeep
---
Currently, I have Windows 7 and Ubuntu 14.04 installed on my system. Now I want to give a try to rpm based linux. So I am going to explain how to install fedora alongside these OS's.

**STEP 1:** First of all make bootable USB of fedora. It can be made bootable by using a tool for windows named: _rawrite32_.

**STEP 2:** After that boot through the USB and select language and proceed with basic steps as specified.

**STEP 3:** Now at the Installation type menu, choose Custom button at the bottom.

**STEP 4:** Choose a free disk drive (e.g. /dev/sda10 ), and select mount point to / and allocate memory to / as per your requirements. Then press Done.

**NOTE:** If you are facing a caution icon or warning like this: "failed to get info", then select disk(from disk and usb) and there will be option called "don't install bootloader" or something similar. Select that and press done. Then if no error occurs then proceed and choose a root password and install.

If you have any problem in the procedure please do comments(also if you're successful). Thank you. **_:)_**
