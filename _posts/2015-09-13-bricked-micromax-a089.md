---
layout: post
title: "Bricked Micromax A089"

postday: 2015/09/13
posttime: 02_40
author: Mandeep
---
Today got to learn new things related to adb, fastboot, recovery mode, factory mode. Actually one of my friend got his phone bricked by accidentally deleting some system file (that was rooted). Firstly downloaded the stock ROM and recovery file. Tried to put them in the memory card but in the recovery mode, the memory card didn't show up. Also after getting the phone attached to the laptop, the phone started charging but it wasn't showing up in the adb devices list. Also tried softwares like SP flash tool, but every time the phone didn't get detected.

Accidentally booted in factory mode (Volume Down+Power button) and finally adb started working. In the factory mode, even the tools like MTK flash tool detected the phone. Now the problem was to send the files from the laptop to the phone using adb. So the command for the same is:

`adb push source-filename destination-folder`

Example:

`

adb push rom.zip /storage/sdcard0`

This command will push (or send) the file rom.zip from the current directory to the /storage/sdcard0 folder which in my case was the internal memory. As the size of the stock ROM was quite large (more than 400MB) and the device didn't have enough free space (internal memory). So have to delete some files and folders from internal memory using adb.

Similarly, there are commands like `adb pull, adb rm, adb ls` etc. that I used while browsing. `adb reboot recovery` to reboot the phone to recovery mode.

Interactively, we may use the command `adb shell` to dive into the shell of the device and run many Linux commands there.

Now the after putting the ROM (or Operating system zip package) to the phone, when booted the phone into recovery mode > Apply update from sdcard > selected the zip > Installation aborted.

So I was stuck at this now. I also tried with SP and MTK flash tools, but still the phone didn't go beyond the logo.

Now it was turn to try to flash custom recovery (tried both CWM and TWRP) into phone, but unfortunately it didn't get installed. It didn't show up in phone (internal storage even) on recovery mode with .img extension. I tried one with zip extension then it showed up but it aborted without successful operation.

Also tried the sideload option after reading the option from the recovery mode namely "apply update from adb". It allows you to send file via the `adb sideload <filename>` from computer and it'll be installed automatically in the phone.

But even this method didn't work and installation aborted.

So the problem didn't solve for now. Actually the phone don't get recognized by the fastboot command. While running fastboot devices, it shows nothing there (tried on both recovery mode and factory mode). But got to learn many things.

**adb** (android debug bridge) can be used to talk to the phone via computer. Tasks like browsing files (copying, removing), installing apk's etc. can be done through it.

**fastboot**, as I read can be used to perform similar kind of tasks as adb like flashing recovery files into phone. Also unlocking the bootloader etc.

**Factory mode,** can be opened with special key combination while turning on the phone. In this case, it was Volume Down + Power button.

**Recovery Mode, **can be accessed similarly as factory mode with Volume Up + Power button. We have to press and hold both buttons for a few seconds. It actually provides you with the options to update firmware, Reset Factory settings (helpful when forget pattern, pincode of lockscreen) etc.
