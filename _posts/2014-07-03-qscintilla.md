---
layout: post
title: "QScintilla"

postday: 2014/07/03
posttime: 22_24
author: Mandeep
---
Qt is cross-platform UI and application development framework. Cross-platform application is that runs on different available platforms like MS Windows, Linux, Mac OSX etc. Scintilla is a free library that has text editing functions. It has many features for easy editing documents like syntax highlighting. Error detection, style, code folding and margin line numbers etc. are some another features. Geany and Notepad++ text-editors are based on Scintilla. QScintilla is a port of the Scintilla C++ editor class to the Qt GUI toolkit. QScintilla includes features especially useful when editing and debugging source code. In simple words,
    
    
    Qscintilla = Qt+Scintilla

![Qscintilla](http://qt.developpez.com/tutoriels/QScintilla/images/image1.png)

Today there was a presentation on QScintilla. In this presentation, we were explained about developing an text editor.

The another presentation was about Thin Client by Gagan and Aseem.  Thin client is a machine with fewer resources such as RAM, processor and graphics card etc. Therefore the presentation was about installing an Operating System like Ubuntu on Thin client.  Thin client depends upon a server to fulfill its resource requirements. In this case, the thin client had 100 to 200 Megabytes of RAM and a very slow processor. Hence they used a Laptop with Ubuntu 12 as a server. Hence any laptop can be made to act as server by installing LTSP. LTSP is Linux Terminal Server Project. _LTSP_ is a Thin Client Solution for Linux operating systems. To install LTSP :

Open terminal and type:
    
    
    sudo apt-get install ltsp-server-standalone

Depending upon the architecture of thin client, run:
    
    
    sudo ltsp-build-client --arch i386

Then in the boot menu of thin client change the following settings:
    
    
    BIOS->integrated peripheral > network controller> enable
    first boot > network
    Save
    Reboot.
    
    
    
    

 

Means you just have to make thin client boot from the network. By default it is disabled. Hence old Desktops at your home that has minimal resources can be made thin clients and can be connected using a wire to your laptop(server).
