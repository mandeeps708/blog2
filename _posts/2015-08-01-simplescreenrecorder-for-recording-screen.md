---
layout: post
title: "SimpleScreenRecorder for recording screen"

postday: 2015/08/01
posttime: 23_56
author: Mandeep
---
SimpleScreenRecorder is a tool that can be used to record the video of the screen. Audio can also be recorded. There are several recording options like full screen recording or selective area, window etc. Just browse to the location to which you want to store the video. Then start video recording. It can also be paused in between. The default shortcut is: Ctrl+R. After you are finished with recording, don't forget to click the 'Save recording' button at the bottom. Now add the ppa to your system:
    
    
    sudo add-apt-repository ppa:maarten-baert/simplescreenrecorder
    
    
    
    

Now install it using:
    
    
    sudo apt-get update && sudo apt-get install simplescreenrecorder
    
    
    
    

It has very good performance. Fast rendering of video. No glitches. Nice tool.

**Update**: If the audio recording doesn't work, you should select the 'Source' of audio to Built-in Audio Analog Stereo. ![simpescr]({{ site.url }}/attachments/simplescreenrecorder.png)
