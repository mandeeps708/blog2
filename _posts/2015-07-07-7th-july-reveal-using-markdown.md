---
layout: post
title: "7th July - Reveal using Markdown"

postday: 2015/07/07
posttime: 11_59
author: Mandeep
---
I searched about creating presentation with markdown. I found reveal-md. Here is the link to my fork: <https://github.com/mandeeps708/reveal-md>

Before installing reveal-md, you should have node installed on your system.

##### Installation
    
    
    npm install -g reveal-md

This command gave some warnings as it is required to be run as root.

After taking some time, finally it installed.

After that I executed the following command to see the demo:
    
    
    reveal-md demo
    
    
    
    

With this the server got started and it automatically opened Chromium browser and displayed the presentation.

I also tested it with some new file and wrote some markdown in it and then executed:
    
    
    reveal-md dpresent.md

Here dpresent.md is the name of the file.

![reveal-md in browser]({{ site.url }}/attachments/reveal-md.png)

The code is:
    
    
    # Welcome
    * Mandeep Singh
    * Computer Science & Engineering
    
    
    
    
    
    ---
    
    
    
    
    
    ## Guru Nanak Dev Engineering College
    
    
    
    
    
    > Testing and Consultancy Cell
    
    
    
    
    
    Note: speaker notes FTW!
    
    
    
    

And to open speaker notes, the keyboard shortcut is 's' key.
