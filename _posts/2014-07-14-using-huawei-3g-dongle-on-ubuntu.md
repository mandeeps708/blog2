---
layout: post
title: "Using Huawei 3G Dongle on Ubuntu"

postday: 2014/07/14
posttime: 02_12
author: Mandeep
---
This may be happen to you that you plugged in a 3G Dongle and your Internet is not working directly. You'll be going to smash that dongle. We have to setup a connection before using a 3G Dongle with our system. As I have done this with Huawei Dongle. Follow these steps:

  * Go to your network icon on right top bar. Click on Edit Connections. If you can't find any icon addressing "network" then press the "Start" of "Super" button on your keyboard to open a search bar or simply click the 1st icon of your launcher in the left pane. Search for "network" and click on "Network Connections".
  * Now click on Add
  * Choose Mobile Broadband.
  * Click next and choose your device or click "Any device".
  * Choose your Country e.g. India.
  * Select your Network provider i.e. the sim brand e.g. Aircel.
  * Click continue.
  * Select your plan. Usually it is GPRS.
  * Click on continue and Apply.
  * A Connection box will appear. Just in the "General" Tab check option "Automatically Connect".
  * Then click ok and close.
  * Now plug your Huawei Dongle with a Aircel sim (or any other) in it.
  * Let the system detect it and it will mount the storage.
  * A new CD icon will appear on the launcher on left side. Click on it.
  * After opening it open the folder MobilePartner Linux.
  * There will be a file called Linux Driver 4.19...tar.gz or something like that.
  * Open that file and extract the "driver" folder to your desktop.
  * Now close any other window and open terminal (shortcut Ctrl+Alt+T).
  * Go to the "driver" folder.
    
    
    cd /home/your-user-name-here/Desktop/driver

To change the permissions run
    
    
    chmod -R 555 *

After that type:
    
    
    ./install /home/mandeep/Desktop/

Press the Enter key if prompted and then:
    
    
    sudo cp usbmod /sbin

Type your password and press Enter. Now,
    
    
    sudo cp startMobilePartner /sbin

After that type:
    
    
    sudo cp 10-Huawei-Datacard.rules /sbin

Now type:
    
    
    reboot

to restart your system. Don't plug out the Dongle from your system.

  * After restart, let the system detect the dongle and mount it. Then click on the network icon above on top right side.
  * Click on it. If you have enabled the Networking then you will see the Huawei (or the name of connection you given).
  * Click on it and let it connect and wait for atleast 10 seconds.

If the Internet works, that's excellent. But if not, that's fine. Click on the Edit connections and try to change the network provider settings and reconnect. If that too doesn't work, then read the README file properly. There is also a documentation named "Linux driver Tool User guide.doc". Open that, read and enjoy.
