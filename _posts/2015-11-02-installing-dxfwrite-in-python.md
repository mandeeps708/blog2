---
layout: post
title: "Installing Dxfwrite in python"

postday: 2015/11/02
posttime: 11_13
author: Mandeep
---
One can easily install dxfwrite with pip in python.

Just fire this :
    
    
    pip install dxfwrite

Then test this simple example:
    
    
    from dxfwrite import DXFEngine as dxf
    
    
    
    
    
    drawing = dxf.drawing('test.dxf')
    drawing.add_layer('LINES')
    drawing.add(dxf.line((0, 0), (1, 0), color=7, layer='LINES'))
    drawing.save()
    
    
    
    

It will create a file test.dxf in the current directory. Open this in a software like Qcad or something that's capable of viewing dxf files.
