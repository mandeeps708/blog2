---
layout: post
title: "Mystery of s/mistake/correct"

postday: 2014/07/15
posttime: 01_01
author: Mandeep
---
My six weeks training has already over. There were a lot of thing to learn. One of them was while you have written a incorrect word or spelling in the email, then you get it corrected from other members. For example, if I wrote "This is awsomme" or anything that is grammatically incorrect, then anyone will correct it like "s/awsomme/awesome". Yeah, pretty good. But I didn't know what does that "s" means. One day I thought about it to search it, but then I forgot. Now today I typed a command in the terminal : _**vimtutor**_

This command will teach us everything about vim from beginner level. Vim is text editor. Vim is contraction of _Vi iMproved_. Vim was first publicly launched in 1991. Vi is "_Visual in ex_". The vi command in ex turns the line editor ex into visual mode. Vim is open source text editor.

Now coming to the topic, there is a command in Vim called "substitute" command. It you find any error in the code, it can be corrected with this command. To use this command, you should know what is the suspect (or incorrect) word or the word you want to replace.

For example, if a document opened in Vim contains a line "microsoft has bought nokia".  Now you wish to correct the word "microsoft" with "Microsoft". Then in Vim, type the following command:
    
    
    :s/microsoft/Microsoft
    
    
    :s/nokia/Nokia

The result will be
    
    
    Microsoft has bought Nokia

It will correct the word. I know it can be done using the "x" button in Normal Command Mode. But this command is helpful when there are more number of substitutions to be made e.g. "google is a search engine and I use google to google anything I don't know.". Then all the words "google" can be replaced by "Google" with the following command:
    
    
    :s/google/Google/g

This "g" here will correct all words "google" in the line. I think "g" is meant for global.

And the command:
    
    
    :%s/old/new/g

will replace all "old" words with "new" in the whole file. Hence it is very easier because it is done without touching the mouse.

The command :
    
    
    :1,500s/mandeep/Mandeep/g

will substitute "Mandeep" for "mandeep" in the range of lines 1 to 500. You can set the range as per your requirement. Well, all this can be better learned from the vimtutor command in the terminal.

Now, I think I know, why my training mates are using "s". :P :)
