---
layout: post
title: "Root Samsung Galaxy Y GT-S5360"

postday: 2015/01/23
posttime: 00_50
author: Mandeep
---
Today one of my friend asked me,"How do I root my phone?". I asked his model name and searched for the solution for his phone. Follow the procedure:

1\. It's better to have your phone charged over 50%.

2\. You should have a backup of your important data with which you can't compromise to get deleted. Backup your contacts(from the contacts>import/export>export to sd card), messages or another important data in phone memory.

3\. Download this zip file : [update.zip](http://forum.xda-developers.com/attachment.php?attachmentid=1621554&d=1357478494) or from my storage : [update.zip](https://drive.google.com/file/d/0B7A1mjqF9sKsWV9FME41RVJ2OTg/view?usp=sharing).

4\. Move this file update.zip to the memory card(SD Card).

5\. Switch off the phone.

6\. Press and hold the 3 keys simultaneously: Volume Up + Home + Power Key. Then you will see the Samsung logo then release the hold.

7\. Now your phone will gets booted in 'Recovery Mode'. There will be various options available in Red text. You touch screen will not work during this 'Recovery Mode'. You have to work with volume buttons to scroll up and down and home button to select a particular option.

8\. Now select the option: 'Apply update from sd card'.

9\. Now it will show you, your Memory Card data. You have to select the update.zip file we have copied to the Memory Card (Press Home button to select).

10\. It will take a second and now choose the option 'Reboot now' using Home button.

11\. Switch on the phone normally using only Power button. After booting up, you can see a new applicaton Superuser in the application menu (That ensures that the process completed and the phone is rooted).

12\. Now download 'Smart Booster Pro' application from the Google Play Store. Then open it after installing. You will be prompted for the 'superuser access'. Then give superuser access to this application. Then click on 'App manager'. Then on 'Backup & Uninstall'. Swipe left for accessing System Applications. Then you will see around 100 apps. You can now uninstall unwanted System applications like Gmail, Email, Maps, Internet (Default Browser if you own another one) etc. Click on any application and select Uninstall. If it does not work, then do it again. Or you can simply Disable the application from showing on the Launcher( app menu) and restricting its RAM and Internet access. You will not be able to use it again unless you enabled it in similar way. Just don't uninstall everything without having prior knowledge of the consequences. Please do some search and then take action or comment below this post if you need any help.

13\. This phone comes with 289 MB of RAM. After uninstalling a few system applications, it uses only 70 to 80MB of RAM and 160-170 MB RAM is still free.

So give it a try, it feels better.
