---
layout: post
title: "Set battery's critical level in Ubuntu"

postday: 2015/01/04
posttime: 00_51
author: Mandeep
---
One day I selected 'Power Off' option in the 'When power is critically low' section. There's no other option to select. It means that when the battery is critically low then system will be shut down. It depends upon the critical level of battery of system whether it is 15% or 10%.

![power management ubuntu]({{ site.url }}/attachments/power.png?w=300)

*power management*

Follow these simple steps to set critical level of battery:

**STEP 1**: Install the dconf-editor by typing in the terminal:
    
    
    sudo apt-get install dconf-editor

**STEP 2**: Now start dconf-editor (by typing 'dconf-editor' in the terminal or in Dashboard).

**STEP 3**: Browse to:
    
    
    Org -> gnome -> settings-daemon -> plugins -> power

![dconf-editor > power](http://localhost/wp-content/uploads/2014/12/dconf-editor-300x250.png)

*dconf-editor*

You may also search for 'power' from starting by typing Ctrl+F and then type power in it and click next several times to get the following screen:

**OR**

try the following commands:
    
    
    gsettings set org.gnome.settings-daemon.plugins.power percentage-critical 4
    gsettings set org.gnome.settings-daemon.plugins.power percentage-action 3
    gsettings set org.gnome.settings-daemon.plugins.power use-time-for-policy false

The first command will set to show "Battery is critically low" warning on 4% battery level. (Change accordingly). The second command will set to shutdown on 3% battery level.

Now close the dconf-editor and enjoy.
