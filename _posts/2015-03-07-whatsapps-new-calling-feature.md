---
layout: post
title: "Whatsapp's new calling feature"

postday: 2015/03/07
posttime: 01_47
author: Mandeep
---
Since Whatsapp has announced its calling feature a few days back, I got it today. For getting this feature we need to update Whatsapp to the latest version. You may update it from http://whatsapp.com .

Yeah you may experience that even after updating you are not getting the calling option anywhere. Okay... You need to be invited from someone who already have this feature. My friend had this feature and he invited me by calling directly from Whatsapp. Then after sometime the UI of Whatsapp changed automatically and the call icon appeared on the top of every contact. But you may call only ones that have the new version of Whatsapp.

I tried it with 3 persons and my experience says that it is a bit laggy. There is a 2 to 3 seconds gap between the voice of sender side and receiver side. It may also occur due to the slow Internet connection on either side. So try it and give your review.![whatsapp call]({{ site.url }}/attachments/contacts-call.png?w=180) ![call]({{ site.url }}/attachments/call.png?w=180)
