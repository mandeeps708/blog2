---
layout: post
title: "Garbled text while pasting in vim"

postday: 2015/12/03
posttime: 18_52
author: Mandeep
---
If you paste text in **vim** and sometimes it distorts the text like some extra indentation gets added. This is a pain for those who work with python :P This problem occurs when there is auto indent or smart indent enabled. There might be other reasons. To solve this simply type in command mode:
    
    
    :set paste
    
    
    
    

this will put vim in paste mode. And try pasting the same text now. It works. To disable the paste mode, type:
    
    
    :set nopaste
    
    
    
    

You may also try **:set noai** and **:set nosi** for disabling _autoindent_ and _smartindent_ respectively. When you are done pasting, do** :set ai** and **:set si**
