---
layout: post
title: "Installing LAMP on ArchBang"

postday: 2015/12/23
posttime: 21_06
author: Mandeep
---
First of all update and upgrade your system:
    
    
    sudo pacman -Syu
    
    
    
    

##### 

#### **APACHE**

**Install apache**
    
    
    sudo pacman -Syu apache
    
    
    
    

 

**Configuring Apache**:
    
    
    **Note**: Always create a backup file before editing files critical files. You can copy the particular file before editing (use cp command).
    
    
    
    

 

Edit **/etc/httpd/conf/extra/httpd-mpm.conf** to have this:
    
    
    <IfModule mpm_prefork_module>
    
    
    
    
    
    StartServers        2
    
    
    
    
    
    MinSpareServers     6
    
    
    
    
    
    MaxSpareServers     12
    
    
    
    
    
    MaxRequestWorkers   30
    
    
    
    
    
    MaxRequestsPerChild 3000
    </IfModule>

 

Edit **/etc/httpd/conf/extra/httpd-default.conf**:
    
    
    KeepAlive Off

 

**To make Apache auto-start at boot:**
    
    
    sudo systemctl enable httpd.service
    
    
    
    

 

Now type **localhost** in your browser. This should show _Index /_ in browser because no file is currently there.

 

**Now configuring virtual hosts.**

Edit file: **/etc/httpd/conf/httpd.conf** and edit the line containing **DocumentRoot** and set it to:
    
    
    DocumentRoot "/srv/http/default"
    
    
    
    

 

Now go to almost bottom of the file i.e. near _line 500_ (or 92% of file) and **Uncomment** the line:
    
    
    Include conf/extra/httpd-vhosts.conf
    
    
    
    

 

Now open **/etc/httpd/conf/extra/httpd-vhosts.conf **and paste the following:
    
    
    <VirtualHost *:80> 
         ServerAdmin root@localhost
         ServerName localhost
         ServerAlias localhost
         DocumentRoot /srv/http/localhost/public_html/
         ErrorLog /srv/http/localhost/logs/error.log 
         CustomLog /srv/http/localhost/logs/access.log combined
                <Directory />
                   Order deny,allow
                   Allow from all
                </Directory>
    </VirtualHost>

You can replace localhost with your domain name.

 

Now create the directories according to the configuration done above:
    
    
    sudo mkdir -p /srv/http/default
    sudo mkdir -p /srv/http/localhost/public_html
    sudo mkdir -p /srv/http/localhost/logs
    
    
    
    

 

Now restart Apache:
    
    
    sudo systemctl restart httpd.service
    
    
    
    

It will still show you an _Index / _page when you type in 'localhost' in the browser. If you want a custom web page to appear while doing the same, then go and createa new file:
    
    
    /srv/http/localhost/public_html/index.html 
    
    
    
    

And add some text that you want it to show. That's it.

 

#### **MySQL**

In Arch, MariaDB can be used alternatively of MySQL.

So, installing the **mariadb**, **mariadb-clients** and **libmariadbclient** packages using the following command:
    
    
    sudo pacman -Syu mariadb mariadb-clients libmariadbclient
    
    
    
    

Install MariaDB data directory:
    
    
    sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
    
    
    
    

All mysql command will work as usual with MariaDB.

Now to make it start on boot:
    
    
    sudo systemctl start mysqld.service 
    sudo systemctl enable mysqld.service  
    
    
    
    

At last, to have secure installation, run:
    
    
    mysql_secure_installation
    
    
    
    

And set root password (Press Enter for current password) and then enter new password for root. Remove remote login, anonymous users, test database and reload privileges tables.

You can now test the installation by firing this command:
    
    
    mysql -u root -p
    
    
    
    

It will prompt for your password that you have just set. Then you can try creating database, tables as usual.

 

#### **PHP**
    
    
    sudo pacman -Syu php php-apache
    
    
    
    

 

Edit **/etc/php/php.ini **and search for following lines and uncomment and modify them:
    
    
    error_reporting = E_COMPILE_ERROR|E_RECOVERABLE_ERROR|E_ERROR|E_CORE_ERROR
    
    
    
    
    
    log_errors = On 
    
    
    
    
    
    error_log = /var/log/php/error.log
    
    
    
    
    
    max_input_time = 30
    
    
    
    
    
    extension=mysql.so

 

Create the log directory for PHP and give the Apache user ownership:
    
    
    sudo mkdir /var/log/php
    sudo chown http /var/log/php
    
    
    
    

Enable the PHP module in the `/etc/httpd/conf/httpd.conf by adding these lines at the end:`
    
    
    #Dynamic Shared Object (DSO) Support
    
    
    
    
    
    LoadModule php5_module modules/libphp5.so
    
    
    
    
    
    # Supplemental configuration
    # PHP 5
    
    
    
    
    
    Include conf/extra/php5_module.conf
    
    
    
    
    
    # Located in the <IfModule mime_module>
    
    
    
    
    
    AddType application/x-httpd-php .php
    
    
    
    
    
    AddType application/x-httpd-php-source .phps

In the same file, comment out the line `**LoadModule mpm_event_module modules/mod_mpm_event.so **`by **adding** a **`#`** in front, and add the line:
    
    
    LoadModule mpm_prefork_module modules/mod_mpm_prefork.so
    
    
    
    

And add the following line:
    
    
    LoadModule mpm_prefork_module modules/mod_mpm_prefork.so
    
    
    
    

**Restart** Apache server:
    
    
    sudo systemctl restart httpd.service

 

#### **phpMyAdmin**

Install phpMyAdmin using the following command:
    
    
    sudo pacman -S phpmyadmin
    
    
    
    

Enable the **`mysqli`** and `**mcrypt** `by editing `/etc/php/php.ini` and uncommenting the following lines:
    
    
    extension=mysqli.so
    extension=mcrypt.so

You need to make sure that PHP can access `/etc/webapps`. Add it to `open_basedir` in `/etc/php/php.ini` if necessary:
    
    
    open_basedir = /srv/http/:/home/:/tmp/:/usr/share/pear/:/usr/share/webapps/:/etc/webapps/

Edit **/etc/httpd/conf/extra/phpmyadmin.conf** file:
    
    
    Alias /phpmyadmin "/usr/share/webapps/phpMyAdmin"
    
    
    <Directory "/usr/share/webapps/phpMyAdmin">
        DirectoryIndex index.php
        AllowOverride All
        Options FollowSymlinks
        Require all granted
    </Directory>

 

`Edit **/etc/httpd/conf/httpd.conf**`:
    
    
    # phpMyAdmin configuration
    Include conf/extra/phpmyadmin.conf

 

Restart Apache again:
    
    
    sudo systemctl restart httpd.service

 

Now click on <http://localhost/phpmyadmin/> and see phpMyAdmin opening in the new tab.

 

**Sources**:

<https://www.linode.com/docs/websites/lamp/lamp-server-on-arch-linux>

<https://wiki.archlinux.org/index.php/PhpMyAdmin>
