---
layout: post
title: "FreeCAD Plugins"

postday: 2016/06/01
posttime: 07_32
author: Mandeep
---
There are basically two ways to add functionality to FreeCAD. 1\. Workbenches 2\. Macros

#### Workbenches

Workbenches can be thought of the set of tools grouped together to perform a specific task. Workbenches can be C++ or Python modules. The C++ ones written earlier are bundled with FreeCAD itself like _Part_, _PartDesign_, _Mesh._ More information regarding this can be found at <http://www.freecadweb.org/wiki/?title=Workbenches>.

Workbenches that are being written in Python are managed at <https://github.com/FreeCAD/FreeCAD-addons> so that they can be separated out from FreeCAD and plugged in later on. Hence, it becomes easy to handle Python modules as they don't need compilation. Python workbenches are _Arch_,_ openSCAD_, _ship _etc.

Some are hybrid (combination of C++ and Python) like _Path_, _Draft _etc.

#### Macros

Macros can be visualized as single-file python scripts. These are having .FCMacro file extension. There are two ways to record macros.

**1\. Interactive**: Go to _Menu_ > _Macro_ > _Macro recording_ > Enter any name > Click '_Record_' > Do whatever you want to do and then click _Macro_ > _Stop macro recording_. Read more: <http://www.freecadweb.org/wiki/index.php?title=Macros>

**2\. Python**: Go to _Menu_ > _Macro > _ _Macros > Create > _Enter file name > Click '_OK_' > Write python code and save (Ctrl+S).

Macros are located at the FreeCAD wiki: <http://www.freecadweb.org/wiki/index.php?title=Macros_recipes>. Later, it was thought to port those macros to GitHub: <https://github.com/FreeCAD/FreeCAD-macros>. As one doesn't need to know programming to create Macros, hence a normal user finds it difficult to use git and GitHub and most macro authors don't have a GitHub account so they didn't move the macros there. And the wiki is very easy for people to put their macros.

 

Hence, the idea is to implement a plugin manager that would be managing both Workbenches and the Macros without giving much hassle to the users to install them, via a single interface.

Currently, the installation directory for **~/.FreeCAD/Mod** (in GNU/Linux at least). To see the User Data directory, go to FreeCAD Python console and paste:

> print FreeCAD.getUserAppDataDir()

This will print the directory where to store the plugins. **Mod** is for containing the workbenches and the **Macro** directory is for storing the user created macros.
