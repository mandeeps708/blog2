---
layout: post
title: "Change Origin of a git repository"

postday: 2015/07/16
posttime: 16_44
author: Mandeep
---
If you want to change the URL of a repository to which it points to, then this post is for you. If you try the same command:
    
    
    git remote add origin http://github.com/USERNAME/OTHERREPOSITORY.git
    
    
    
    

then it is not gonna work and you will probably get error like this:

> fatal: remote origin already exists.

List your existing remotes in order to get the name of the remote you want to change
    
    
    git remote -v
    
    
    
    

Change your repository's remote url with command:
    
    
    git remote set-url origin https://github.com/USERNAME/OTHERREPOSITORY.git
    
    
    
    

Substitute the values of USERNAME and OTHERREPOSITORY with yours.
