---
layout: post
title: "Wake On LAN"

postday: 2015/08/09
posttime: 18_21
author: Mandeep
---
If you have one computer at a location on a network and you want to turn it on remotely i.e. without physically present at that location, then there is something called Wake On LAN (WOL). But the condition is the computer's hardware must support WOL and it should be provided continuous power supply and connected to the network.

For WOL to work, the target computer must have the functionality of WOL in it. On most computers, one may find it in the BIOS > Power Management >Wake on LAN or Wake-up by PCI card. If you find it there, then Enable it.

Once WOL enabled on first computer, note down the MAC address of the computer. You may do it on Linux by typing command:
    
    
    ifconfig

MAC address will be the 12 digit code separated by colons (usually) and is labelled as HWaddr as after running above command in terminal.

On a windows pc, run
    
    
    ipconfig /all

to see the same.

For more about setting up WOL, see: https://www.mythtv.org/wiki/Wake-on-LAN

Now, you need to set up the other computer from which you want to wake up the first one. I am using Linux (Ubuntu 14.04) currently, so on that I've compiled WOL. Here is the link to the tool:  http://sourceforge.net/projects/wake-on-lan/

After downloading, extract the tar.gz file and go to that directory in terminal. First run
    
    
    ./configure

and after that run
    
    
    make

and finally run
    
    
    make install

and you are done. Read the INSTALL file in the same directory for further help.

After it installs, run command:
    
    
    wol <mac:address:here>

 
