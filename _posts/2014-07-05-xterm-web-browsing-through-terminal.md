---
layout: post
title: "Xterm: web browsing through terminal"

postday: 2014/07/05
posttime: 02_56
author: Mandeep
---
First of all open the Terminal by pressing Ctrl + Alt + T.

First of all type:
    
    
    sudo apt-get install w3m w3m-img

w3m is a text-based web browser. But w3m-img can be used to view images.

Now to Xterm be installed on your system, type:
    
    
    sudo apt-get install xterm

xterm is a terminal emulator for windows systems. It may be included with various linux distributions as built-in package. But if it is not there in your's then try above command.

After all setup, type in terminal :
    
    
    xterm -bg white -fg black

Here -bg white argument is for changing the background of xterm to white, and -fg black is for changing font color to black. For default looks, you can just type "xterm" command. In a few seconds, you will came to know why I have selected these colors.

Then a new window will be open like below one:

![open xterm](http://mandeep7.files.wordpress.com/2014/07/1.png?w=300)

 

Then in the xterm window type :
    
    
    w3m google.com

![opening google](http://mandeep7.files.wordpress.com/2014/07/2.png?w=300)

Then you will see Google.com opening like below image. If you can't see image on website, then double click anywhere on the xterm screen. Xterm does not support .gif format images (animated). This page is similar to the original google homepage. That's why I have chosen background color white by using man xterm command.

![google.com](http://mandeep7.files.wordpress.com/2014/07/3.png?w=300)Press "Tab" key to move the cursor through the hyper-links (links). Then press tab keys and arrow keys to navigate to the text input bar of google. Then press Enter to input the text. Then you will see what you type is shown on the bottom left corner. Then press Enter.

![entering text](http://mandeep7.files.wordpress.com/2014/07/6.png?w=300)Now use Arrow keys (or Tab) to go the link "Google Search". Or just press Tab and Press Enter. Then page will open with appropriate results like below:

![search results](http://mandeep7.files.wordpress.com/2014/07/5.png?w=287)You can see the result as above. It is also showing the map to tcc, gndec. Then press Tab key (or arrow keys) to toggle the cursor through links, and press Enter to open a website. In this case, I have clicked on images link to open images related to tcc, gndec. So here are the results:

![images results](http://mandeep7.files.wordpress.com/2014/07/7.png?w=300)

Right Click on any image and choose the required option. Double click an image to open large size. For example, if you want to save the image press "Shift+i". Then give a path to save the image on your system or just press Enter. A Download List Panel will be open as shown below:

![Downloads](http://mandeep7.files.wordpress.com/2014/07/9.png?w=300)Default directory for downloaded images in /home/mandeep/ directory. Replace mandeep by your username. You can also play online music from the websites like [this. ](http://djpunjab.com)This will open the vlc media player for online streaming.

The same procedure will also work in case of Terminal (ctrl+alt+T) , but the problem is that we can't see any images in terminal because it does not support images.
