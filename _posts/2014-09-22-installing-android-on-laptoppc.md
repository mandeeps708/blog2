---
layout: post
title: "Installing ANDROID on Laptop/PC"

postday: 2014/09/22
posttime: 20_48
author: Mandeep
---
These days almost everyone have a android smartphone. But have you ever imagined to install android as an operating system on your PC or laptop. Anyways, this article is about installing android operating system on your laptop.

To continue, you need to fulfill following requirements:
    
    
    1. A compatible pc or laptop
    2. Free space on Hard disk drive
    3. Android-x86 ISO 
    4. USB or CD

#### Download Android x86

First of all, download .iso file of android x86 from their website. It's around 300 mb in size. I am working with android-4.4-RC1.iso file in this article.

#### Preparing USB

You need to create bootable USB of your android x86. Insert USB and in windows open UNETbootin software. You can download it easily from internet, it's just around 5 mb in size. Open Unetbootin, locate .iso file of android and select your USB drive. Then create a bootable USB (pendrive).

#### Creating Separate Partition

You can use 'gparted' or 'disks' softwares in Ubuntu to create a partition. Note down it's partition number. For example, sdaX. Here X is the your drive number. In my case it is sda8. I have formatted it to ext4 filesystem. You may use windows to shrink a separate drive.

#### Installation

Then Restart your system and make your pc boot through USB by changing BIOS settings, give your USB the priority to boot first. Then you will see grub menu. There you will see several entries like "Try android without installation", "Install android" etc. Then choose "Install Android". Choose your hard disk drive. It will list all available drives. Choose your free drive carefully that you previouly created. In my case it is /dev/sda8. Choose format to ext3. Now it will ask installing grub on disk. If you are using Windows only then click Ok to install grub. If you have already installed Ubuntu or you are using grub then Click SKIP. Then it will start installing android and a few seconds, it will ask you whether to Start android or Reboot. Click on Reboot.

If you have chosen "Install Grub" then you will see android's grub menu containing android and Windows entry. If you have chosen "SKIP" to install Grub as you have already Ubuntu's Grub installed then you will not see Android in the list. You have to manually add android to grub menu. I will explain you in the next article.

**Update**: <../../../../2016/03/24/adding-android-to-grub/>
