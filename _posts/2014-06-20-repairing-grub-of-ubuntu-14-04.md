---
layout: post
title: "Repairing Grub of Ubuntu 14.04 (With Internet)"

postday: 2014/06/20
posttime: 03_09
author: Mandeep
---
> #**How to Repair grub when you have installed the Windows 8 or other operating system over Ubuntu.**

First of all, I want you to get familiar with grub. Grub is basically a  loader that is executed before operating system loads. Grub includes the different operating systems' entries to choose from. I had Ubuntu 14.04 on my laptop. Then I tried to dual boot my laptop with Windows 8. When I installed Windows 8 and restarted then the grub was not there.

> #**Requirements:**

So first of all you must have a USB or pen-drive. You must have a Linux distribution .iso image file.

> #**Preparing the USB:**

Then you must have to access Windows operating system whether it is on your own system or your friend's.  Now download any Live USB creator from Internet. There are lot of Live USB creators like Universal USB creator, LInux Live USB creator (LiLi), Unet Bootin etc. I prefer you to use Linux Live USB creator. Download it and install it on your Windows 8 or 7 or xp operating system. Plug your USB into system and backup your data of the USB. Run the software and then select your USB drive carefully. Then locate the .iso image file through it and wait for the checking process. Tick the format USB option. Click the "Lightening" button to create Live USB. It will take some time. Please wait for it to complete. When it gets completed then restart the computer.

> #**Booting Live USB:**

Before startup there is an option shown on black screen i.e. Press Esc key to interrupt or something like that and press that key and change the boot order of the system to your USB. You have to press f9 button (in my case) to open boot order. Then the system will automatically boot up from the USB. Then It will load the live operating system.

> #**Repairing Grub:**

Once you are ready then open the terminal by pressing shortcut "ctrl+alt+t" from your keyboard. You must be connected to internet with good speed (3G or Wifi is recommended). Then type the following command:
    
    
    sudo add-apt-repository ppa:yannubuntu/boot-repair

This command is for adding boot-repair to the repository To update your repository type,
    
    
    sudo apt-get update

To install boot-repair,
    
    
    sudo apt-get install -y boot-repair

Once the installation completes, run boot-repair on terminal by typing :
    
    
    boot-repair

Then it will scan for your system for some time. Then it will show you two options:

  *     Recommended Repair

  *     Create a BootInfo summary

Now click on Recommended Repair option. Then it will start repairing the grub and most probably it will find your lost ubuntu from your hard drive.

During the process, the boot-repair utility will open some GUI (Graphical User Interface) windows to ask you some information about your drive on which Ubuntu was installed. If you can differentiate between the hard drives then press tab to go to menu and press space to select a drive and press tab and enter to continue. If you don't know on which drive your Ubuntu was, then select all drives. Then it will install grub to all hard drives. Make sure all the commands runs correctly, if a command doesn't successfully executes then re-write (Press the Up arrow button to view last entered commands) the command and press Enter.
