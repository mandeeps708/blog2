---
layout: post
title: "Battery-alert"

postday: 2015/09/16
posttime: 11_53
author: Mandeep
---
From a past few days, this happened many times with me that the system warns me to plug-in the charger else it'll shut down soon. But who cares? :P Okay, sometimes I don't. And that resulted into direct shutdown. So I thought of making a script of alerting me with a sound (and may be a dialog) when the battery level reaches a certain level (>80% and <20%). After starting with it, the first thing I needed to know was the current battery level of the system through the command line. So, one of achieving it is by using the `upower` command. Then I used made comparisons with battery level. For playing sound, I used play command that can be used after installing sox (`apt-get install sox`).

Link to github repository is <https://github.com/mandeeps708/battery-alert>
