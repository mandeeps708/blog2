---
layout: post
title: "Creating figures in dxfwrite"

postday: 2015/11/03
posttime: 11_27
author: Mandeep
---
The task is to create a Drain drawing and then calculate the area of cutting and filling.

Firstly, I tried to create lines using coordinates.

See this for the code:

[https://github.com/mandeeps708/PyDrain](https://github.com/mandeeps708/PyDrain/commit/0b0cac91518066de99138f3f3acf68f418eb350a)

Next, I'll try it by getting input from a CSV file.

Follow the official documentation here: http://dxfwrite.readthedocs.org/en/latest/index.html
