---
layout: post
title: "Some unknown git commands"

postday: 2015/11/19
posttime: 11_35
author: Mandeep
---
To loose the unstaged changes to a file (i.e. reverting to the committed changes) while using git:
    
    
    git checkout <file-name>
    
    
    
    

It will not loose another unstaged or staged changes.

To show a log of commits:
    
    
    git log
    
    
    
    

To show detailed log:
    
    
    git log -p
    
    
    
    

To see the detailed log about a particular file:
    
    
    git log -p coord.csv
    
    
    
    

 

To see a file at a specific commit, we have to first check the commit-id or hash using the git log commands explained above. You may copy a first few characters of a commit-id. The syntax for this is:
    
    
    git show commit-id:filename
    
    
    
    

For instance,
    
    
    git show f1a95c65900f5:coord.csv
    
    
    
    

This will show the file at that specific commit. You can also direct its output to a new file using the '>' operator.

And to show all changes at that particular commit:
    
    
    git show f1a95c65900f5
    
    
    
    

So to revert to all changes at a commit state:
    
    
    git revert f1a95c65900f5
    
    
    
    

To revert one file to a particular commit:
    
    
    git checkout <hash> <file path>
    
    
    
    
