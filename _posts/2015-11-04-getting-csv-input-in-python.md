---
layout: post
title: "Getting CSV input in Python"

postday: 2015/11/04
posttime: 11_43
author: Mandeep
---
Now it takes input from the CSV file and create lines.

[https://github.com/mandeeps708/PyDrain](https://github.com/mandeeps708/PyDrain/commit/07e841dfe37acd570125a4e0ae211e69b5f89df5)

Now it takes two lines (i.e. 4 coordinates, or 2 points) as input at once, that are necessary to create a line with dxfwrite line function.
