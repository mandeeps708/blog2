---
layout: post
title: "Create a .deb file from currently installed package"

postday: 2014/09/13
posttime: 17_46
author: Mandeep
---
Yeah, if you ever installed a software for example firefox. And you have lost its source code or its .deb file and you want to reuse it on another computer. Then you had to have download it again. But after some search I found out a command to create a .deb file from a software that is currently installed. The command is :

sudo dpkg-repack firefox
