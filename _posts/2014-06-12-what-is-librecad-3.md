---
layout: post
title: "What is LibreCAD 3?"

postday: 2014/06/12
posttime: 13_09
author: Mandeep
---
LibreCAD is a Computer Aided Design tool that is used to create 2D designs. LibreCAD 3 is a GSoC (Google Summer of Code) project. LibreCAD 3 can be cloned using github from your terminal through the link 

`https://github.com/LibreCAD/LibreCAD_3`
