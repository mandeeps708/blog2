---
layout: post
title: "Day 7 - MATLAB Functions"

postday: 2015/06/09
posttime: 11_31
author: Mandeep
---
Today I started with the [Robotic Vision](https://moocs.qut.edu.au/courses/791/learn) Project. This project is all about having a worksheet input into MATLAB and make computer identify the colors and shapes of the 2D objects drawn on the worksheet. Hence first of all, the course introduces how to work with functions.

I created a simple function myself to start with. I named it msum.m.
    
    
    function addition = msum(a,b)
    addition = a+b;
    end
    
    
    
    

What this function does is to add two variables a and b that it takes as arguments.

Then following the course, I created a new function named getColor.m that can be used to identify objects and different colors (Red, Green, Blue) and create binary images.
    
    
    function color = getColor(im, clr)
    Y = sum(im,3);
    redChroma = im(:,:,1) ./ Y;
    greenChroma = im(:,:,2) ./Y;
    blueChroma = im(:,:,3) ./Y;
    
    
    
    
    
    redBinary = redChroma > 0.4;
    greenBinary = greenChroma > 0.4;
    blueBinary = blueChroma > 0.4;
    
    
    
    
    
    switch(clr)
    case 'red'
    color = redBinary;
    case 'blue'
    color = blueBinary;
    case 'green'
    color = greenBinary;
    
    
    
    
    
    otherwise
    fprintf('Invalid color option. Use red, blue or green in single quotes only.');
    end
    
    
    
    

 

This function basically takes two arguments im the input image and clr (the binary image having specified color as 1). First of all chromaticity coordinates are calculated and then the separated colors are compared with threshold value of 0.4 which returns the binary image highlighting that only color.

Now I am working on identifying the shape of the objects.
