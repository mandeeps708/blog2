---
layout: post
title: "Day 17 - Compiling"

postday: 2015/06/19
posttime: 11_33
author: Mandeep
---
Today I installed weboctave on Experimental server (http://exp.raiandrai.com/~admins6/weboctave) and got the same error like on my localhost. So searched on Internet, asked on IRC (#octave) about the dispatch error but didn't find relevant solution.

Also got to know that octave got updated to 4.0 version. Currently it was 3.8. So I downloaded the [octave-4.0.0.tar.xz](///home/mandeep/Downloads/octave-4.0.0.tar.xz) and extracted it.

First make sure to install following packages:
    
    
    apt-get install gfortran f2c libblas-dev liblapack-dev
    
    
    
    

Now traverse to the extracted directory:
    
    
    cd octave-4.0.0
    
    
    
    

Now execute command:
    
    
    ./configure

This will take enough time.

After it got successfully executed then execute make command:
    
    
    make

Now make will take its time and let it complete and then install using:
    
    
    sudo make install
    
    
    
    

Then finally it will get installed on your system.

Now when you type 'octave' in the terminal, it will execute the 4.0.0 version of octave even if I'd installed 3.8 version of it.

Actually octave is symbolic link to /usr/local/bin/octave-4.0.0

 

After updating octave to 4.0 version, it introduces new error when we run weboctave. The errors are:

Errors:
    
    
    octave: X11 DISPLAY environment variable not set
    octave: disabling GUI features
     'dispatch' undefined 
     called from
         at line 99 column 1

I solved first error of X11 by replacing octave-cli with octave. It can be achieved by renaming the files. It's always a good practice to make a backup of files you are going to change so I stored these somewhere else.

Now there is only one error:
    
    
    'dispatch' undefined 
     called from
         at line 99 column 1

 
