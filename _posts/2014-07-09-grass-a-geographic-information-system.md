---
layout: post
title: "GRASS - A Geographic Information System"

postday: 2014/07/09
posttime: 11_25
author: Mandeep
---
![GRASS GIS Software](https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Grass_GIS.svg/150px-Grass_GIS.svg.png)There was a presentation on a GIS software named GRASS by Ankur. GRASS is an open source Geographical Information System (GIS). Full form of GRASS is Geographic Resources Analysis Support System. This software can be used to manipulate raster, vector, image processing etc. The software imports a file containing some random points located at some location by surveying. In this case, the file was created in Microsoft Excel and contains points by specifying x,y,z co-ordinates. In the vector processing, the points are plotted as per the locations of points. Then in the raster processing, a surface is created. We can view it in 3D view and it can be easily rotated as per requirements.

 
