---
layout: post
title: "Pandoc: convert document formats"

postday: 2016/05/02
posttime: 01_24
author: Mandeep
---
I had to submit some text to mediawiki. As it supports special markdown, but I had a file on Google Docs. Initially, I looked up for some online tools for conversion. For them I downloaded the Doc in html format. But that didn't work. For example: <http://pandoc.org/try>. It doesn't support docx as input. But it support HTML. But it didn't like much the HTML produced from Google Doc.

The thing that finally worked for me was that I installed pandoc on my system and then did the conversion.

Ubuntu guys can install is simply using:

> _sudo apt-get install pandoc_

Then I read the manpage and found the -t flag useful. I searched for "mediawiki" in the manpage and that did the job.

The actual command that I fired was:

> _pandoc -t mediawiki input.docx > output_

Then there will be a file named **output **will be created in the current directory in mediawiki style markdown format.

**Update**: More about pandoc can be seen at <http://pandoc.org/> and the formatting help for mediawiki content can be found at <https://www.mediawiki.org/wiki/Help:Formatting>
