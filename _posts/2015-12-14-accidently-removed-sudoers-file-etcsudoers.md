---
layout: post
title: "Accidently removed sudoers file (/etc/sudoers)"

postday: 2015/12/14
posttime: 03_15
author: Mandeep
---
In case you have deleted the /etc/sudoers file and can't use sudo anymore (or another users can't) then here are the commands:
    
    
    pkexec apt-get purge sudo
    pkexec apt-get install sudo
    
    
    
    
