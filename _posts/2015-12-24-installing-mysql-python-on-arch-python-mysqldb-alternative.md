---
layout: post
title: "Installing mysql-python on Arch (python-mysqldb alternative)"

postday: 2015/12/24
posttime: 02_08
author: Mandeep
---
If you want to install **python-mysqldb** on Arch and couldn't find the package then try the following commands:
    
    
    sudo pacman -Ss mysql-python
    
    
    
    

and/or
    
    
    pip install mysql-python
    
    
    
    
