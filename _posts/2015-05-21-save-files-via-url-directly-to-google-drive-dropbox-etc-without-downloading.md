---
layout: post
title: "Save files via URL directly to Google Drive, Dropbox etc. without Downloading"

postday: 2015/05/21
posttime: 23_58
author: Mandeep
---
> http://ctrlq.org/save/

Yes this is the website that provide the facility to save files directly to Google Drive, Dropbox, ftp server etc.(and some more) without downloading and uploading them again.

Just paste the link of file to be downloaded and then choose the destination. Verify your account with the service (like Google).

Then it will be saved!!!

I tried 10-15 pdf files and it worked. But it couldn't save a zip file worth 237mb in size(IDK the reason). Please tell me something in comments if you used it.
