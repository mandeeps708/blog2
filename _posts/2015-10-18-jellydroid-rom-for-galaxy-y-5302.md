---
layout: post
title: "JellyDroid ROM for Galaxy-y 5302"

postday: 2015/10/18
posttime: 18_30
author: Mandeep
---
I had the rooted galaxy-y 5302 phone on which the custom ROM is to be installed.

**STEP 1**: First of all, download the CWM recovery from here: <http://forum.xda-developers.com/showthread.php?t=1971119>

I downloaded this one: [vM00CWM.zip](http://forum.xda-developers.com/attachment.php?attachmentid=1453188&d=1351941119)

**STEP 2**: Now download the ROM file. <http://www.mediafire.com/download/990810fryvrnc39/JellyDroid+OS+GT+S5302+By+DSB.zip>

152 MB in size.

**STEP 3**: Now put both the files in Internal memory. As the recovery mode was not showing the files in sdcard. You may copy the ROM file to the memory card (sdcard) as well.

**STEP 4**: Power off the phone. Now press Volume UP + HOME + Power buttons for a few seconds and then it will boot in recovery mode.

**STEP 5**: Choose Apply for sdcard (using volume buttons) press Home button to select it. Similarly browser to your vMooCWM.zip file there and select and install that.

**STEP 6**: After installation, CWM recovery will be shown there. Now you can backup your stock ROM using backup and restore option there.

**STEP 7**: Now you can Choose Choose update from sdcard (as 'apply update from sdcard' option was giving error). Now select the ROM file. Press on Yes option by scrolling down.

**STEP 8**: It will start installing and finish in a few seconds.

Here is the original xda-developers thread: http://forum.xda-developers.com/showthread.php?p=42590792#post42590792

After the installation, Reboot the system normally. Now it will stuck for sometime at black screen. Don't panic. After a minute or two, it will start.
