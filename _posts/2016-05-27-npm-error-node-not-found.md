---
layout: post
title: "NPM Error: node: not found"

postday: 2016/05/27
posttime: 14_44
author: Mandeep
---
I tried to install a package (slap) via npm. For this, you have to install nodejs (may be through the package-manager). And I ran the following command npm install and got errors. One of those was **node: not found**. Here is the complete error:

[sourcecode language="bash" wraplines="true" collapse="true"] ○ → sudo npm install -g slap@latest [sudo] password for mandeep: /usr/local/bin/slap -&gt; /usr/local/lib/node_modules/slap/slap.js

> runas@3.1.1 install /usr/local/lib/node_modules/slap/node_modules/runas > node-gyp rebuild

/bin/sh: 1: node: not found gyp: Call to 'node -e "require('nan')"' returned exit status 127 while in binding.gyp. while trying to load binding.gyp gyp ERR! configure error gyp ERR! stack Error: `gyp` failed with exit code: 1 gyp ERR! stack at ChildProcess.onCpExit (/usr/share/node-gyp/lib/configure.js:354:16) gyp ERR! stack at emitTwo (events.js:87:13) gyp ERR! stack at ChildProcess.emit (events.js:172:7) gyp ERR! stack at Process.ChildProcess._handle.onexit (internal/child_process.js:200:12) gyp ERR! System Linux 4.4.0-22-generic gyp ERR! command "/usr/bin/nodejs" "/usr/bin/node-gyp" "rebuild" gyp ERR! cwd /usr/local/lib/node_modules/slap/node_modules/runas gyp ERR! node -v v4.2.6 gyp ERR! node-gyp -v v3.0.3 gyp ERR! not ok /usr/local/lib `-- (empty)

npm ERR! Linux 4.4.0-22-generic npm ERR! argv "/usr/bin/nodejs" "/usr/bin/npm" "install" "-g" "slap@latest" npm ERR! node v4.2.6 npm ERR! npm v3.5.2 npm ERR! code ELIFECYCLE

npm ERR! runas@3.1.1 install: `node-gyp rebuild` npm ERR! Exit status 1 npm ERR! npm ERR! Failed at the runas@3.1.1 install script 'node-gyp rebuild'. npm ERR! Make sure you have the latest version of node.js and npm installed. npm ERR! If you do, this is most likely a problem with the runas package, npm ERR! not with npm itself. npm ERR! Tell the author that this fails on your system: npm ERR! node-gyp rebuild npm ERR! You can get information on how to open an issue for this project with: npm ERR! npm bugs runas npm ERR! Or if that isn't available, you can get their info via: npm ERR! npm owner ls runas npm ERR! There is likely additional logging output above.

npm ERR! Please include the following file with any support request: npm ERR! /home/mandeep/npm-debug.log npm ERR! code 1 [/sourcecode]

To solve this, I created a symlink /usr/bin/nodejs to /usr/bin/node and it worked fine. Here is it how to do that:

sudo ln -s /usr/bin/nodejs /usr/bin/node
