---
layout: post
title: "15th June"

postday: 2014/06/15
posttime: 15_44
author: Mandeep
---
Today was sunday, so at home I worked on LibreOffice Draw. I made some designs in it. It's a nice software to make 2D and 3D design. In this software, we can convert 2D shapes like rectangle, circle or line into 3D shapes by using 3D rotation view tool. It is a vector graphics editor like Microsoft Visio. Microsoft Visio premium is an expensive program that nearly costs $1000. But libreOffice is a free open source software.

![libreOffice Draw](http://mandeep7.files.wordpress.com/2014/06/rings.png?w=240)
