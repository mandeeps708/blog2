---
layout: post
title: "Moodle on Production Server"

postday: 2016/03/01
posttime: 03_16
author: Mandeep
---
Recently I installed [Moodle](https://moodle.org/) 3.0 on a production server. It is currently up at [learn.gdy.club](http://learn.gdy.club).

> _Moodle_ is a course management system (CMS) - a free Open Source software package designed to help educators create effective online courses.

First of all, [Vigas](http://www.vigasdeep.com/) and me decided to have some domain name for that. Out of exam, edu, test, study etc. "learn" seemed to be best. So it was selected.

Due to the fresh install of the nginx server, the phpmyadmin was not working. So first had to resolve that. So I did:

> sudo ln -s /usr/share/phpmyadmin /usr/share/nginx/html/phpmyadmin

to make it work.

One thing I didn't know was that if we point to some directory in nginx, if it don't have the index file (like index.html) then it will show 403 Forbidden error instead of showing the file structure. It is the default behavior.

For nginx config, just had to do something like this:

> server_name learn.gdy.club; root        /home/moodle/public_html;

I used <https://docs.moodle.org/30/en/Step-by-step_Installation_Guide_for_Ubuntu> to set up moodle.

I created a new user for moodle and did everything in that user's home directory and public_html directory instead of playing with /opt and /var directories.

Also accessing learn.gdy.club was giving me 404 error due to DNS issues. So I added an entry to my /etc/hosts file:

> 202.164.53.116     learn.gdy.club

After going through the basic installation steps, it leads to the configuration prompts which we have to fill. After installation it opens: http://learn.gdy.club/my. But the problem with this was that it wasn't showing any images and CSS on the page. After checking console output, it showed 404 errors to http://learn.gdy.club/lib/javascript.php/1456342577/lib/requirejs/require.min.js and similar files.

Then [vigas](http://vigasdeep.com) fixed it by disabling the "Slash arguments" on <http://learn.gdy.club/admin/settings.php?section=http>.

But there was a note:

_'Slash arguments' (using PATH_INFO) is required for SCORM packages and multiple-file resources to display correctly. If your web server doesn't support 'slash arguments' and you are unable to configure it, this setting can be disabled, though it will result in things not working._

_Note: The use of 'slash arguments' will be required in future versions of Moodle._

So we have to use "Slash arguments" to make it work properly.

Solution was there:

https://docs.moodle.org/dev/Install_Moodle_On_Ubuntu_with_Nginx/PHP-fpm#Moodle

i.e. adding a line to the nginx config file:

> rewrite ^/moodle/(.*\\.php)(/)(.*)$ /moodle/$1?file=/$3 last;

But it didn't work. So I removed the words "moodle" from the above line (2 occurrences) as we didn't have "moodle" directory in the public_html.

Also set up "System Paths After Install" which requires du, aspell, dot (graphviz package), ClamAV antivirus etc.

That's all. Thank you.
