---
layout: post
title: "Get html form values into python through CGI"

postday: 2015/09/20
posttime: 01_36
author: Mandeep
---
I modified the HTML file and made the python file containing those HTML tags. But when applying bootstrap CSS to that file, it was not picking up the CSS file from the same cgi-bin folder. So I placed it in the localhost and it worked.

So, to get the form values through CGI, first I did:
    
    
    import cgi, cgitb
    
    
    
    
    
    cgitb.enable()
    
    
    
    
    
    form = cgi.FieldStorage()
    
    
    
    

 

For example, the name attribute of an input field is defined as 'Soil_type' in the HTML tags. Hence, we can get the value entered in that input field by:
    
    
    value = form.getvalue('Soil_type')
    
    
    
    

Also, I got some added a line:
    
    
    os.system("octave program.m > output")
    
    
    
    

The above code was for executing a shell command through python. It was working on my system but giving some error on the server. It may be due to some different configuration on the server than my laptop.

Github Repository link: <https://github.com/mandeeps708/cocgi/>
