---
layout: post
title: "14th June"

postday: 2014/06/14
posttime: 14_53
author: Mandeep
---
Today there was a presentation about apache server by Balpreet Kaur. She explained the way to install apache, how to run the simple C++ program in the Web browser like firefox using cgi script. She explained about the installation of apache on different distributions like Ubuntu 13.10 and 14.04. So overall it was a nice presentation. Then our mentor assigned the some new tasks to our team in groups. Me with [Gurpinder Singh](http://gurpindersingh751.wordpress.com/) were assigned the task of studying the working of LibreOffice Draw. Libre Draw is a software used to design 2D and 3D drawings. Libreoffice Draw is already installed with some of the Linux distribution. My one is Ubuntu 14.04 and it includes LibreOffice Draw.
