---
layout: post
title: "Day 10 - getShape of circles"

postday: 2015/06/12
posttime: 11_54
author: Mandeep
---
After getting binary image (of any color like red, blue etc.) from getColor function, now the work is to identify the shapes in the binary image. What I did till now is:
    
    
    im = iread('myOwnRV.jpg', 'double');
    
    
    
    
    
    im2 = igamm(im, 'sRGB');
    
    
    
    
    
    r = getColor(im2, 'red', 0.7);
    
    
    
    
    
    idisp(r);
    
    
    
    

![binary image]({{ site.url }}/attachments/bin.png)Hence we got a binary image having 7 blobs (including black background). We can check this using:
    
    
    >> b = iblobs(r);
    >> b
    
    
    
    
    
    b =
    
    
    
    
    
    (1) area=7781344, cent=(1221.8,1635.0), theta=1.58, b/a=0.748, class=0, label=1, touch=1, parent=1, perim=11420.0, circ=0.834
    (2) area=34380, cent=(543.1,1246.2), theta=-3.10, b/a=0.883, class=1, label=2, touch=0, parent=1, perim=912.8, circ=0.577
    (3) area=17899, cent=(839.5,2499.3), theta=-2.42, b/a=0.995, class=1, label=3, touch=0, parent=1, perim=551.0, circ=0.824
    (4) area=23115, cent=(869.4,1136.5), theta=1.60, b/a=0.991, class=1, label=4, touch=0, parent=1, perim=588.8, circ=0.932
    (5) area=67787, cent=(1477.8,1131.1), theta=2.30, b/a=0.992, class=1, label=5, touch=0, parent=1, perim=1070.2, circ=0.827
    (6) area=11658, cent=(1822.8,1467.1), theta=1.63, b/a=0.898, class=1, label=6, touch=0, parent=1, perim=522.9, circ=0.596
    (7) area=54089, cent=(1878.1,2108.8), theta=-2.72, b/a=0.988, class=1, label=7, touch=0, parent=1, perim=905.8, circ=0.922
    
    
    
    

Now I want to detect the shapes from this binary image.

First I started with the 'circle' shape. We know that the circularity for circle is 1. So we take near about 1 in if statement. And we can see clearly in image that there are 2 circles and also there are 2 blobs having values near about 1 above.

So here is my getShape.m function:
    
    
    function ans = getShape(im, shape)
      [L,mx] = ilabel(im);
      b = iblobs(im);
      array = zeros(1,mx); %created a matrix for storing labels of matching blobs
      for i = 1:mx
        if(shape=='circle' & b(i).circularity>=0.9 & b(i).circularity <=1.1)
          array(i) = b(i).label;
        end
      end
      exclude = array(array~=0); %exclude the extra zeros added
      ans = b(exclude); %here the resulting blobs's features are stored
    end
    
    
    
    

To use this function we must be in same directory of where the function is stored (in MATLAB). We can use this function as:
    
    
    >> getShape(r,'circle')
    
    
    
    
    
    ans =
    
    
    
    
    
    (1) area=23115, cent=(869.4,1136.5), theta=1.60, b/a=0.991, class=1, label=4, touch=0, parent=1, perim=588.8, circ=0.932
    (2) area=54089, cent=(1878.1,2108.8), theta=-2.72, b/a=0.988, class=1, label=7, touch=0, parent=1, perim=905.8, circ=0.922
    
    
    
    

Now to make a bounding box around the blobs (circles):
    
    
    >> idisp(r);                  %display the binary image
    >> ans.plot_box
    
    
    
    

The result is shown as:

![circles are bounded by box]({{ site.url }}/attachments/bounding.png)
