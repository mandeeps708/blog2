---
layout: post
title: "Installing Cairo Dock"

postday: 2014/06/28
posttime: 22_33
author: Mandeep
---
Cairo Dock is a beautiful interface Launcher. It is designed to be light, fast and customizable. It has many transition effects. Extra widgets can be added from official website **[http://glx-dock.org/**](//glx-dock.org/). Now coming on to installation,

You can install Cairo-Dock easily by going to Ubuntu Software Center and searching (Ctrl+F) about "cairo dock" and press 'install'.

You can also install it using terminal using the following steps:

> **#STEP 1:**

Open terminal (Ctrl+Alt+T) and enter the following command :
    
    
    sudo add-apt-repository ppa:cairo-dock-team/ppa

Enter your password.

> **#STEP 2: **
    
    
    sudo apt-get update

> **#STEP 3:**
    
    
    sudo apt-get install cairo-dock cairo-dock-plug-ins

You can start it by typing: cairo-dock or you can search it (Start Button) and search for "cairo dock".

 

![cairo](http://mandeep7.files.wordpress.com/2014/07/cairo.png?w=300)

But when you will restart your system, then it will not be there. For cairo-dock to appear always even after restart, you can add it "Startup Applications". To do this, Press Start button and search for "startup applicaton" and open it. Click on "Add".![startup menu](http://mandeep7.files.wordpress.com/2014/07/startup.png?w=300) A new window will be pop-up. In this window, enter any name you like. I have entered "Bottom dock". In the command box, write cairo-dock and press Save and close the dialog and that's it.

You can change any effect by right clicking on any icon of the dock and select Cairo-Dock and then Configure. A new window will be open and you can select a variety of customization. For instance, select "Icon Effects" menu under the Appearance Tab. Check on any effect and click on apply and that's it.
