---
layout: post
title: "Scan documents via mobile camera - OCR"

postday: 2015/10/16
posttime: 11_50
author: Mandeep
---
You might have wondered about scanning a document or a some pages of a book and store them as digital documents. So here is how to do it.

You need a good quality mobile phone camera to be able to take good quality picture that can be clearly understood.

Now there are many mobile applications for Android that helps to scan images. [Textfairy](https://play.google.com/store/apps/details?id=com.renard.ocr&hl=en) is an application that enhances the clicked picture and extracts text from it. The extracted text was not much reliable, but it's OK. Another application is CamScanner. The limitation of these apps is that I couldn't do the batch processing after clicking the pictures. I found one application i.e. Droid scan that did the batch operation. I applied it to 88 images and it took a lot much time. When I reduced the number of images to 5, then it completed within some minutes, but the output was not correct and some images were distorted.

On Linux, we may use the tesseract tool to obtain the text from an image (scanned or clicked). To install tesseract, use the command below, which will also install the data for english language for OCR.
    
    
    sudo apt-get install tesseract tesseract-ocr-eng
    
    
    
    

After it got installed, try it on an image using the following command:
    
    
    tesseract input.jpg output.txt -l eng
    
    
    
    

Here the input.jpg is your image from where you want to extract the text. The output.txt is the target output file that will be created with the text extracted from image. And the -l eng is for specifying the language.

Tesseract is very accurate as per my experience. Read more on [OCR](https://en.wikipedia.org/wiki/Optical_character_recognition) at <https://en.wikipedia.org/wiki/Optical_character_recognition>

 
