---
layout: post
title: "Installing Django 1.7 on Ubuntu 14.04"

postday: 2014/10/29
posttime: 01_10
author: Mandeep
---
First of all you need to Install Python (if not)

Now Type in terminal:
    
    
    sudo apt-get install python-setuptools

##### Installing Django 1.7

  * Go to http://djangoproject.com/
  * Download latest release 1.7.1 .tar.gz file (on right side of website).
  * After finished downloading, now open terminal and type:
  *     cd Downloads

(or your download destination directory)
  *     tar -zxvf Django

and press tab and it will autocomplete.
  *     cd Django (press tab)

  * Then
    
        sudo python setup.py install

  * Then it will take a few seconds to complete.
  * Then after completion, check if it is installed or not:
  * in terminal, type:
    
        python

  * then
    
        >>> import django
    >>> django.get_version()

The output will be:
    
        '1.7.1'

##### WHAT NEXT?

###### Tutorials:
    
    
    http://tangowithdjango.com
    
    
    
    
    
    and visit http://djangoproject.com for official Documentation.
    
    
    
    
    
    http://tutorial.djangogirls.org/
    
    
    
    

**To Uninstall Django**, install pip:
    
    
    sudo apt-get install python-pip

Then ,
    
    
    pip uninstall Django
