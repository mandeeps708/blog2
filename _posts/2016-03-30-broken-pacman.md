---
layout: post
title: "Broken Pacman"

postday: 2016/03/30
posttime: 03_49
author: Mandeep
---
Hello there! I broke my archbang recently. Let me tell you how it went. I was doing upgrade via pacman and suddenly electricity failure and it shut down immediately.

For those who don't know what pacman is: [Pacman](https://wiki.archlinux.org/index.php/pacman) is a package manager for [Arch Linux](https://www.archlinux.org) (Linux distribution) as _apt-get_ is for Ubuntu.

On next boot, many apps like chromium etc. stopped working.

Running **chromium** from console revealed:

_/_usr_/lib/chromium/chromium: error while loading shared libraries: /_usr_/lib/_libpulse_.so.0: file too short_

**Firefox **gave some security error something like "_Your connection is not secure_".

_The owner of www.google.co.in has configured their website improperly. To protect your information from being stolen, Firefox has not connected to this website. _

_www.google.co.in uses an invalid security certificate. The certificate is not trusted because the issuer certificate is unknown. The server might not be sending the appropriate intermediate certificates. An additional root certificate may need to be imported. (Error code: sec_error_unknown_issuer)_

 

Then I thought of doing an update via pacman, but running any pacman command result into an exec error:

zsh**: exec format error: **pacman

On another reboot, I noticed a message (kernel messages): "_**failed** to start _intializes pacman_ keyring_". So at this point, I realized that there is something wrong with the keyring.

Then tried many things like:

_pacman -Syyuu_

_pacman -S pacman_

_pacman -Scc_

Removed _/var/lib/pacman/db.lck_

Removed _/var/lib/pacman/sync_

Then I got a link: <https://wiki.archlinux.org/index.php/pacman#pacman_is_broken_beyond_repair>

It contained a link to a post: <https://bbs.archlinux.org/viewtopic.php?id=95007>

It said to manually configure the packages: `openssl, libarchive, libfetch, pacman`

So I downloaded the _.pkg.tar.xz_ files and fired the command below:

_sudo tar -xvpf ${pkgname} -C / --exclude .PKGINFO --exclude .INSTALL_

where ${pkgname} should be replaced by the package_name.pkg.tar.xz e.g. openssl, pacman etc.

So this overrode the existing binaries, I guess and pacman started working.

But while installing something via _pacman _like "pacman -S **_pacman_**", then it gave error like:

_:: Proceed with installation? [Y/n] Y _

_(1/1) checking keys in keyring _

_[######################] 100% _

_downloading required keys... _

_:: Import PGP key 20235p273o23o4-somekeyhere, "**<Some-name>**__ <**some-email**>", created: 2011-06-06? [Y/n] Y _

_:: File /var/cache/pacman/pkg/pacman-5.0.1-2-x86_64.pkg.tar.xz is corrupted (invalid or corrupted package (PGP signature)). _

_Do you want to delete it? [Y/n] Y _

_error: failed to commit transaction (invalid or corrupted package (PGP signature)) _

_Errors occurred, no packages were upgraded._

So it was importing very old keys.

Tried: _pacman-key --populate archlinux_

Got:_ _

_==> Appending keys from archlinux.gpg..._

_gpg: no valid OpenPGP data found._

_==> Updating trust database..._

_gpg: no need for a trustdb check_

Then tried:

_sudo rm -rf /etc/pacman.d/gnupg_

_sudo pacman-key --init_

These were working but attempting installing something still gave that PGP error thing.

 

##### **What solved my problem?**

_sudo pacman-key --refresh-keys_

Followed:

<http://www.cupoflinux.com/SBB/index.php?topic=2959.0>

Then pacman started installing things but it still gave some errors while doing that:

_(1/1) upgrading gnome-terminal                     [######################] 100%_

_ldconfig: File /usr/lib/libssh.so is empty, not checked._

_ldconfig: File /usr/lib/libass.so is empty, not checked._

_ldconfig: File /usr/lib/liboslquery.so is empty, not checked._

and a long list of errors like above. Those errors mean that you have a missing installation of certain packages. If you find and reinstall those packages by:

_pacman -Syyf <PACKAGE_NAME_HERE>_

you'll reinstall and dismiss those errors. But -f option is no longer supported, one may use --force instead. <https://bbs.archlinux.org/viewtopic.php?id=85480>

 

Hence I used the following command:

_sudo pacman -Qnq | pacman -S --force -_

<https://bbs.archlinux.org/viewtopic.php?id=209493>

What it did was to find and reinstall all the installed packages (around 1GB of downloading).

Finally got everything working.

 
