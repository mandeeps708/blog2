---
layout: post
title: "adb not detecting the phone!"

postday: 2016/11/12
posttime: 16_42
author: Mandeep
---
### What is it?

adb is abbreviated for "Android Debug Bridge". It comes with the Android Development SDK but it can be installed separately too. It allows users to send commands to the Android phone via PC. It will be helpful in cases if you are tinkering with your phone and reached some nearly bricked condition.

In my case: I have a Zuk Z1 and I have installed TWRP recovery and Cyanogen 13 installed as of now. There might be some case or something went wrong that I can't boot into my phone and even recovery isn't working. Then booting the phone into bootloader might help. But I didn't find any way to boot into bootloader without going into recovery. And suppose recovery isn't working too. Then adb can help you here. You may use adb to boot into bootloader and then flash recovery and the OS later on.

##### Installation

On debian based systems e.g. Ubuntu, you may install it like:
    
    
    sudo apt-get install android-tools-adb android-tools-fastboot

If it didn't work, then see [here](http://askubuntu.com/questions/659880/ubuntu-15-04-adb-fastboot-impossible-to-install). It will get you adb and fastboot.

##### My Problem:

The problem was that after installing adb, I connected the phone and fired the command:

> _adb devices_

but it wasn't listing my device.

So after searching a bit, I got a solution.

First you need to know the vendor id of your device. Search the Internet for that.

Other thing you can try is connect your phone to Linux PC and from terminal type:

> _lsusb -v_

and look for your device and then look for "idVendor" field. For example mine is: **0x2b4c**

Another thing you may try is to use following command from terminal:

> _fastboot devices_

after booting the phone into the bootloader.

So coming back to my problem, my device wasn't getting listed on running **adb devices**.

The solution is that you need to create/edit a file _**~/.android/adb_usb.ini **_and write it as:

> _# ANDROID 3RD PARTY USB VENDOR ID LIST -- DO NOT EDIT. _
> 
> _# USE 'android update adb' TO GENERATE. _
> 
> _# 1 USB VENDOR ID PER LINE. _
> 
> _# ie 0x2207 _
> 
> _0x2b4c_

And place the vendor id at the end, like I did above. Save file and run **adb devices** again. It will list the device but it will be unauthorized. Now your phone should pop up a permission box to allow the connection. Allow it (tick always).

Then do the following to restart the adb server (with sudo).
    
    
    adb kill-server
    
    
    
    

and afterwards
    
    
    adb start-server
    
    
    
    

Try reconnecting the phone after disconnecting if it didn't work.
