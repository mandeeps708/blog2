---
layout: post
title: "Vim: Add some text at the start/end of each line"

postday: 2017/03/31
posttime: 00_18
author: Mandeep
---
Suppose, you have a file with 1000 lines. You want to add some text at the beginning/end of each line. You won't think of doing it manually (actually, I can't stop you). Here, I am telling you how to save you a lot of time.

For example, we have the following lines in the file:

**_Name_**

**_Email_**

**_Phone Number_**

**_Address_**

And you want your output to be:

**"Name",**

**"Email",**

**"Phone Number",**

**"Address",**

and so on.

In Vim, you can simply type:
    
    
    :%s/$/",/

It will place **", **at the end of each line.

Similarly, for beginning:
    
    
    :%s/^/"/

It will place **" **at the beginning of each line.

Hence, we used $ for appending to end and ^ for prepending.

You can also do this for a few lines by visually selecting them with **V **and then pressing **:** and type the command
    
    
    s/^/"/

Skip the "%" because we do it while doing things globally across the file. So it will look like:
    
    
    :'<,'>s/^/"/
    
    
    
    

Another method, if you want to type some text interactively on a couple of lines:
    
    
    Ctrl+V

Select a few rows (downside, pressing **j**). After selecting the position in couple in all rows, press:
    
    
    Shift + i

They type what you want. Then press the Esc key:
    
    
    Esc

Now, wait 1 second. You'll see same text to be replicated over all rows you selected.

Please share in comments, if you even easier method to do the similar.
