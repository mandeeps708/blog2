---
layout: post
title: "Using git for past"

postday: 2015/11/28
posttime: 15_46
author: Mandeep
---
If you have some changes that you want to be shown as it were committed in past, then you may use this command:
    
    
    GIT_AUTHOR_DATE='Mon Jan 26 16:32:33 2015 +0530' 
    GIT_COMMITTER_DATE='Mon Jan 26 16:32:33 2015 +0530'
    git commit -m 'commit message here'
    
    
    
    

It will commit the staged files committed as per the date given. Take care of the +0530, it should be your timezone.

If you want to change a commit from the log list then get the workaround here: http://stackoverflow.com/a/3896112/3784226
