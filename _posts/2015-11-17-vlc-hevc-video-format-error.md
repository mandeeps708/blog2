---
layout: post
title: "VLC hevc video format error"

postday: 2015/11/17
posttime: 00_00
author: Mandeep
---
If you get this error on any video/movie then you are going to solve it with three commands in the terminal (Ctrl+Alt+t).![vlc-hevc-error]({{ site.url }}/attachments/vlc-hevc-error.png)

Just open terminal and type:
    
    
    sudo apt-add-repository ppa:strukturag/libde265 
    sudo apt-get update 
    sudo apt-get install vlc-plugin-libde265
