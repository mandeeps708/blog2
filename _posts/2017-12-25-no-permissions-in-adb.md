---
layout: post
title: "no permissions in adb"

postday: 2017/12/25
posttime: 19_35
author: Mandeep
---
Hi everyone,

Read my older post [adb not detecting the phone!](../../../../2016/11/12/adb-not-detecting-the-phone/) to get some basic information about what adb is.

Now the problem came when I attached my phone to a freshly installed OS. This issue might only be specific to my OS. But I had to search the solution on the Internet when I faced it again. So thought of writing it myself this time.

Error is:
    
    
    $ adb devices
    List of devices attached
    9bb2ce65 no permissions (verify udev rules); see [http://developer.android.com/tools/device.html]
    
    
    
    

So basically, it's showing the device (9bb2ce65) but I can't send any command (e.g. reboot) to my phone. Because it's written there as "no permissions". And it didn't pop up any dialog box on my phone so that I could allow the permissions.

For solving this, I went to the [link](http://developer.android.com/tools/device.html) (and [this](https://askubuntu.com/a/909227)) mentioned in the above output.

To add yourself to the plugdev group:
    
    
    $ sudo usermod -aG plugdev $LOGNAME

And kill the adb server.
    
    
    $ sudo adb kill-server
    
    
    
    

Start it again, with:
    
    
    $ sudo adb start-server
    
    
    
    

Now the error is gone.
    
    
    $ adb devices
    List of devices attached
    9bb2ce65 device
    
    
    
    

 
