---
layout: post
title: "Day 9 - getColor"

postday: 2015/06/11
posttime: 23_21
author: Mandeep
---
As the course 'Robotic Vision' has been already finished officially on 8th July, I got an email today regarding my certificate of participation in the course by Professor Peter Corke. So I got it today in pdf format.

![robotic vision certificate]({{ site.url }}/attachments/certi.png?w=289)

 

Today I edited the function getColor function to have 3 arguments: input image (im), color that we want (clr) and threshold value (thr).
    
    
    function color = getColor(im, clr, thr)
    Y = sum(im,3);
    redChroma = im(:,:,1) ./ Y;
    greenChroma = im(:,:,2) ./Y;
    blueChroma = im(:,:,3) ./Y;
    
    
    
    
    
    redBinary = redChroma > thr;
    greenBinary = greenChroma > thr;
    blueBinary = blueChroma > thr;
    
    
    
    
    
    switch(clr)
    case 'red'
    color = redBinary;
    case 'blue'
    color = blueBinary;
    case 'green'
    color = greenBinary;
    
    
    
    
    
    otherwise
    fprintf('Invalid color option. Use red, blue or green in single quotes only.');
    end
    
    
    
    

This is done to enhance the image. Every image can't be threshold with a default value.

As I discussed in [previous post](http://wp.me/p3CX0w-aG) that there was a problem of some blobs having area =1 i.e. there are some white dots in the image. To solve this, I used gamma correction.
    
    
    im2 = igamm(im, 'sRGB');
    
    
    
    

Hene, the function can be used as:
    
    
    r = getColor(im2, 'red', 0.7)
    
    
    
    

to extract binary image with highlighted pixels as red. Also note that I used gamma corrected image as input to getColor function and threshold value of 0.7.
