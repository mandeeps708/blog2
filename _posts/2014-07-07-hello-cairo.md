---
layout: post
title: "Hello Cairo !!"

postday: 2014/07/07
posttime: 00_03
author: Mandeep
---
It is the last week of my training. Me with [Gurpinder](http://gurpindersingh751.wordpress.com/) are currently working on cairo libraries to implement hatching concept. Now we are trying to use cairo to implement it. Cairo can be installed by using the following command in terminal:
    
    
    sudo apt-get install libcairo2-dev

Cairo is a library used for creating graphics and can be used with other programming language like C for further implementations. After installation, we must include its header file as:

_#include<cairo/cairo.h>_ in the program. And during execution of program use,
    
    
    gcc hello.c -o hello -lcairo

Here gcc is the compiler, and hello.c is the main source program written in C and -o is used to create executable file whose name is 'hello' and we can change that name to any. We must bind with the library while execution using _-lcairo_. Without it, execution will show errors. Then ./hello to execute the file.

> **#Hello World Program using Cairo**

[code language="objc"] #include <cairo/cairo.h> int main (int argc, char *argv[]) { cairo_surface_t *surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, 240, 80); cairo_t *cr = cairo_create (surface);

cairo_select_font_face (cr, "serif", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD); cairo_set_font_size (cr, 32.0); cairo_set_source_rgb (cr, 0.0, 0.0, 1.0); cairo_move_to (cr, 10.0, 50.0); cairo_show_text (cr, "Hello, world"); cairo_destroy (cr); cairo_surface_write_to_png (surface, "hello.png"); cairo_surface_destroy (surface); return 0; } [/code]

Save this code in a file named hello.c then open terminal. Go to that directory where the file hello.c resides. Then type:
    
    
    gcc hello.c -o hello -lcairo

Then you will notice that there will be an executable file created in the same directory. So execute that using this command:
    
    
    ./hello

Now a image hello.png will be created in the same directory. You can double click on that to open or open it using terminal:
    
    
    eog hello.png
