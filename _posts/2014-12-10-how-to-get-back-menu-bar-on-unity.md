---
layout: post
title: "How to get back Menu bar on Unity"

postday: 2014/12/10
posttime: 22_15
author: Mandeep
---
If you are not having menu bar that is having multiple options like: File, Edit, View, Help etc. on the top of the your windows then this article is for you.

Here are the screenshots:

Pointer over window:

![manu bar :: mandeep singh 7](http://i.stack.imgur.com/Ev5P0.png)

*hover over window*

Pointer over menu bar:

![hidden menu bar](http://i.stack.imgur.com/5T0cE.png)

*hover over menu bar*

You can see the empty title menu bar. There are no options like file, edit, view etc.

So open Terminal (Ctrl+Alt+T) and copy paste these commands one by one:
    
    
    sudo apt-get update && sudo apt-get install --reinstall indicator-appmenu

Next, reload everything:
    
    
    dconf reset -f /org/compiz/ && unity --reset-icons &disown

If not working, then try this command too:
    
    
    sudo apt-get install --reinstall unity ubuntu-desktop

Better Restart your system once after trying these commands:
    
    
    sudo reboot
