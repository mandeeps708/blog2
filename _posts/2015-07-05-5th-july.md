---
layout: post
title: "5th July"

postday: 2015/07/05
posttime: 23_57
author: Mandeep
---
From 30th June my BSNL broadband was shut down. I called them on 1 July and also complained about it. Due to the construction of sewerage in my village, the broadband wire was cut down. So finally it took them approximately 5 days to repair it. So happy to see it working after I woke up.

I pushed some code to test working of github.io. So pushed some web page and CSS to github directory already set up some time ago by me. Actually you need to have a repository with a branch 'gh-pages' e.g. I created repository website and pushed some code to its gh-pages branch. So I can access it by mandeeps708.github.io/website
