---
layout: post
title: "Fetching GitHub submodules"

postday: 2016/06/14
posttime: 01_31
author: Mandeep
---
[Yesterday](../../../../2016/06/13/github-api-returns/), I took the first step towards fetching information from GitHub. Today added code for checking if the repository contains submodules and will return that instance. Then we may use it to access submodule information like name, submodule_git_url etc.

See: <https://developer.github.com/v3/repos/contents/#response-if-content-is-a-submodule>

I used** x.raw_data.get("type") **to check if **x** a submodule or not. Actually **raw_data** contains many other attributes (like html_url, git_url, name, sha, type etc.) that provide additional info. about the instance itself. And for fetching the original repository link, I used: **x.raw_data.get("submodule_git_url")**.

Here is the code for the particular file: <https://github.com/mandeeps708/Github-API-Fun/blob/master/get-submodules.py>

More about repository methods in PyGithub: <http://pygithub.github.io/PyGithub/v1/github_objects/Repository.html>

**Rate Limit**: https://developer.github.com/v3/#rate-limiting

> For requests using Basic Authentication or OAuth, you can make up to 5,000 requests per hour. For unauthenticated requests, the rate limit allows you to make up to 60 requests per hour. Unauthenticated requests are associated with your IP address, and not the user making requests.
