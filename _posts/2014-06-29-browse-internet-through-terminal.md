---
layout: post
title: "Browse Internet through Terminal"

postday: 2014/06/29
posttime: 11_59
author: Mandeep
---
w3m is a free software/open source text-based web browser. It has support for tables, frames, color and inline images on suitable terminals. Generally, it renders pages in a form as true to their original layout as possible.

The name "w3m" stands for "WWW wo miru (WWWを見る?)", which is Japanese for "to see the WWW" where W3 is a numeronym of WWW.
    
    
    sudo apt-get install w3m w3m-img

> **#Basic functionality of w3m:**

First of all, open terminal and type :
    
    
    w3m google.com

![google terminal](http://mandeep7.files.wordpress.com/2014/07/google.png?w=300)

 

You can open any website you like by replacing the string "google.com". It will load the website in a few seconds. Then use "Tab" key to toggle between the links present on the website and Shift+Tab to move cursor to previous link. Press Enter to open a link.

If you want to go to a new URL (website), then Press Shift+U and enter the URL address and press Enter. It will open up in a new tab.

Press S to view different tabs and select the tab you want and press enter to Select the particular tab. You can also delete a tab by pressing Shift+D.

Ctrl+H to view history

You can press Shift+L to show all links present on a web-page. Click on any link by holding Ctrl will open the link in Default browser like firefox. Press O to open Option Setting Panel in which you can change settings like Display Settings, Color Settings, Network Settings, Proxy Settings and many more.

If you want to show images of websites then you should use Xterm because beside showing images, it has some other features like you can Right-click on any page for options. As you can see, we can't see images in previous image like google logo. For images you can open "Xterm". Xterm is basically a Terminal emulator for Windows systems. Type "xterm" in your terminal to run it. If it is not installed on your system, then you can install it by using command:
    
    
    sudo apt-get install xterm

Then try the same commands. You can quit w3m by pressing q button and pressing y for yes.

![gmail-xterm](http://mandeep7.files.wordpress.com/2014/07/gmail-xterm.png?w=300)We can also convert(or save) a web page (.html) into a text file(.txt) by using the following command:

w3m -dump -T text/html mandeep7.wordpress.com > filename.txt
