---
layout: post
title: "Simplifying Boot Menu with BURG"

postday: 2014/09/25
posttime: 14_32
author: Mandeep
---
### Windows 7 Purple Screen Error [Solved]

I recently tried to dual boot my ubuntu 14.04 with Windows 7. For a few times, it worked just fine. But after some days, I faced a problem every time I boot into windows 7 through grub. The name of the problem is "Windows 7 purple screen". When I pressed enter on the Windows 7 entry in the grub, a purple grub like screen keeps on showing all the time. But windows was loading fine in the background, as I could hear the startup sound of windows. But the only thing I could see was a purple screen and nothing else. But sometimes it worked when I hibernated windows 7. But that was not the permanent solution. I also got to hear from my friends that they had the same problem. So I have found a solution about it. Burg. Burg is an alternative to grub. I tried it and it worked perfectly for me.

Install Burg by opening terminal (Ctrl+Alt+T) and then enter the command:
    
    
    sudo add-apt-repository ppa:n-muench/burg -y
    && sudo add-apt-repository ppa:danielrichter2007/grub-customizer
    -y && sudo apt-get update && sudo apt-get install burg
    burg-themes grub-customizer

Open Grub Customizer by searching it on dash or in terminal run command: grub-customizer

![burg1](http://mandeep7.files.wordpress.com/2014/09/burg1.png?w=300)

You will have to enter your password. Now it will say: BURG found! Do you want to configure BURG instead of grub2?, click on 'Yes'.

![burg2](http://mandeep7.files.wordpress.com/2014/09/burg2.png?w=300)

To make Burg as default(over grub), Click Install to MBR (under File).

You can delete extra entries like 'Ubuntu Recovery.

![burg3](http://mandeep7.files.wordpress.com/2014/09/burg3.png?w=300)

In the General Settings, you can increase the time for default boot entry After you are ready, click on Save and then let it update configurations.

![burg4](http://mandeep7.files.wordpress.com/2014/09/burg4.png?w=300)

Now, we're going to change the theme. Type in the terminal:
    
    
    sudo burg-emu

Enter password.

![burg6](http://mandeep7.files.wordpress.com/2014/09/burg6.png)

 

![burg7](http://mandeep7.files.wordpress.com/2014/09/burg7.png)

Press F2 key to open the theme menu and use arrow keys to select your burg theme.

Then restart your system and then see the magic. Windows 7 will boot up successfully. Press F1 to see help menu and F2 to change theme. There are several themes to choose from.
