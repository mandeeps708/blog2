---
layout: post
title: "Git Revert"

postday: 2017/04/20
posttime: 21_37
author: Mandeep
---
**git revert** can be used to revert the changes you did in your git repository. One of the most useful case would be if you have already published your commits and of course, you won't want to do a history rewrite. Then git revert lets you make a new commit cancelling out the effect of particular commit you specify.
    
    
    git log
    
    
    
    

and note down the commit id of the latest commit you want to revert. Let us assume, it's "**b338b072ex1gd89a1a4eba8ge0d56jko**".

And then:
    
    
    git revert b338b072ex1gd89a1a4eba8ge0d56jko
    
    
    
    

Then do **git log** again and see a new commit reverting the changes of last commit. Similarly, it can be done for multiple commits to, or a range of commits.

See more options @ <http://stackoverflow.com/a/4114122>

Video demo: <https://asciinema.org/a/6479>
