---
layout: post
title: "GitHub API Returns"

postday: 2016/06/13
posttime: 22_11
author: Mandeep
---
Hi everyone,

On 11 June, I tried experimenting with parsing the submodules data from <https://github.com/FreeCAD/FreeCAD-addons>.

I started working on PyGithub today (12 June). Actually, it's a third-party library wrapper written in Python for the GitHub API. The day went off struggling with PyGithub to work. I couldn't understand much at the beginning. As the night elapsed, it started working for me. :P

I started with some basic example like the fetching of the names of the files. You may see the repository here: <https://github.com/mandeeps708/Github-API-Fun>. Actually, this repository has also been created using the API itself (without opening github.com in the browser). Finally got slept at around 7 AM.

Now next is to do get if the instance fetched is a submodule or not. It seems to be much easier to be done now.
