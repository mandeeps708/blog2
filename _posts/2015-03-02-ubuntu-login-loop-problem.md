---
layout: post
title: "Ubuntu Login Loop Problem"

postday: 2015/03/02
posttime: 21_40
author: Mandeep
---
**Ubuntu Login Loop Problem**

How the problem occured: Ctrl + Alt + f2 logged in using my credentials. Then I typed:

> startx

to go back to gui, but it didn't work. Then I tried

> sudo startx

and it let me see my wallpaper, desktop icons, side launcher etc. but the top panel was missing in which the indicators(network-manager, battery, music, time and settings etc.) are present. Also Alt+tab was not working. So I opened terminal and top command and by mistake killed compiz. On the next login, the login loop problem occured. I also tried to change the password but it didn't help.

I tried to reconfigure lightdm(display manager for ubuntu) to default settings using:

> sudo dpkg-reconfigure lightdm

So after searching I created a new user and logged in using it. And what about previous settings etc? Then I found another solution: I went back into command line (ctrl+alt+f2) and after login with the victim account:

> cd
> 
> ls -la

The owner was set to root for the .Xauthority directory. Then change the owner to youself:

> sudo chown mandeep:mandeep .Xauthority

And that solved the problem. :)

 

Source: http://goo.gl/ZZTuHf
