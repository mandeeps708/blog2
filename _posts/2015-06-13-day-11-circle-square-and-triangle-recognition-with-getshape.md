---
layout: post
title: "Day 11 - Circle, square and triangle recognition with getShape"

postday: 2015/06/13
posttime: 11_25
author: Mandeep
---
Today I've modified the function getShape to have more shapes identified.
    
    
    function an = getShape(im, shape)
    [~,mx] = ilabel(im);
    b = iblobs(im);
    array = zeros(1,mx); %created a matrix for storing labels of matching blobs
    for i =1:mx
    switch(shape)
    case 'circle'
    if(b(i).circularity>=0.9 && b(i).circularity <=1.1)
    array(i) = b(i).label;
    end
    case 'triangle'
    if(b(i).circularity>=0.57 && b(i).circularity <=0.6)
    array(i) = b(i).label;
    end
    case 'square'
    if(b(i).circularity>=0.80 && b(i).circularity <=0.83)
    array(i) = b(i).label;
    end
    otherwise
    fprintf('Enter valid input');
    end
    exclude = array(array~=0);
    end
    an = b(exclude); %here the resulting blobs's features are stored
    end
    
    
    
    
