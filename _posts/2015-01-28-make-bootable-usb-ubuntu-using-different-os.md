---
layout: post
title: "Make Bootable USB Ubuntu Using Different OS"

postday: 2015/01/28
posttime: 21_54
author: Mandeep
---
If you want to try Ubuntu(Operating System) on your system or if you are having some problems on booting Ubuntu(or having grub error on boot) then read on. You need to create bootable USB of Ubuntu and boot through it and then install it using on screen instructions. Now I am telling you how to create bootable USB.

###### _PRE-REQUISITIES:_

1\. Ubuntu .iso file ([Download Ubuntu](http://www.ubuntu.com/download) ~No need if you already have .iso file)

2\. USB (with capacity 2GB or more)

3\. A Live USB creator software.

You need to have a working Operating System (may be Micro$oft Windows or Ubuntu or Mac OSX), if your system is not working currently then you can ask for your friends to help you.

#### 1\. USING UBUNTU:

If you are already having access to Ubuntu then follow these steps to create bootable USB:

**STEP 1: Insert USB** drive into system.

**STEP 2:** Open the** dash**(Start button on your keyboard) and search for "**Startup Disk Creator**".

**STEP 3:** Click on the "**Startup Disk Creator**" icon to launch it.

**STEP 4**: Click **'Other'** to choose the downloaded ISO file.

**STEP 5:** Select the file and click **'Open'**.

**STEP 6:** Select the USB stick in the bottom box and click **'Make Startup Disk'**.

Now it will start the process and in a few minutes your bootable USB will be ready to serve. :)

 

#### 2\. USING WINDOWS:

If you are having access to Windows OS then these are the steps to follow:

**STEP 1:** Download Universal USB Installer(UUI) from [HERE, ](http://www.pendrivelinux.com/universal-usb-installer-easy-as-1-2-3/#button)and install it and open it.

**STEP 2:** Select Ubuntu from the list. (If you want to make bootable 14.04 version or 14.10 of Ubuntu and it's not there in the list then you may select the Ubuntu 13.10 version)

**STEP 3:** Click on Browse and locate the .iso file of Ubuntu.

**STEP 4:** Choose your USB drive carefully (cross check it by going to 'My Computer' and check drive letter).

**STEP 5:** Tick on Format as FAT32 and then click on Create.

**STEP 6:** Accept the confirmation dialog and your Bootable USB will be ready in a few minutes.

 

#### 3\. USING MAC OS X

I've not tried it but you can check the tutorial from the following link:

<http://www.ubuntu.com/download/desktop/create-a-usb-stick-on-mac-osx>

 

_Source: Ubuntu.com_
