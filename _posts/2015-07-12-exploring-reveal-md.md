---
layout: post
title: "Exploring Reveal-md"

postday: 2015/07/12
posttime: 11_59
author: Mandeep
---
In a[ previous post](../../../../2015/07/07/7th-july-reveal-using-markdown/), I installed reveal-md and tested a demo. Now I was exploring about an easy way to make excellent presentations. I found reveal-md. It's really an excellent tool.

[Reveal-md](https://github.com/webpro/reveal-md/) is a tool to create reveal.js presentations using [Markdown](https://guides.github.com/features/mastering-markdown/). It means we have to write content in markdown and it will get converted into a beautiful reveal.js presentation.

Hence I started testing what capabilities do it have. To do this, I started writing the markdown.

###### Some syntax hacks:

  * '\---' to create a new slide (three hyphens).
  * '\----' to create a nested vertical slide (four hyphens).
  * We can also define our own syntax for new/nested slide etc.
    
    
    Note: This is a note.

The above line can be used to create a note for a slide. To access the notes, press 's' button.'>' to quote a text.

To add an image (that is present current directory of markdown file) into slide, type:
    
    
    ![image](sea.jpg)
    
    
    
    

 

Now I wanted to change transition style, but I couldn't figure out how to do that.

I got the basic idea from the [README](https://github.com/webpro/reveal-md/) of reveal-md that I have to create a reveal.json file into the root where the markdown files are present. So I created reveal.json file and copied all the text given in the reveal.js configuration (read reveal.js readme), but it didn't worked.

So I asked the developer of reveal-md to know 'how to use it'. I created [an issue](https://github.com/webpro/reveal-md/issues/38) on his github repo. Hence, came to know about the usage.

Create a reveal.json file in the directory where the markdown file is present and you can overwrite the defaults. In reveal.json file:
    
    
    {
    "controls": false,
    "progress": true,
    "transition": "zoom",
    "backgroundTransition": "slide",
    "parallaxBackgroundImage": "sea.jpg",
    "parallaxBackgroundSize": "2100px 900px",
    "parallaxBackgroundHorizontal": 200,
    "parallaxBackgroundVertical": 50
    }
    
    
    
    

Now execute reveal-md dpresent.md where dpresent.md is your markdown file. Now a browser window will open. I suggest to open the same link in Chromium (not working fine in firefox). It gives the 'zoom' transition-style and the controls (4 arrows at bottom-right end) will be hidden, progress bar will still appear, also parallax effect will be there using the image 'sea.jpg' that is present in the current directory and try changing the pixel values.

  *  Change the transition speed of a slide by:

<section data-transition-speed="slow">Choose from three transition speeds: default, fast or slow!</section>

 

  * To change the background transition along with the background color:

 

<section data-background="#FE642E" data-background-transition="fade">Bye.</section>

Explore may other attributes on the [Reveal.js configuration page](https://github.com/hakimel/reveal.js#configuration).

Now I'm going to try some other options like the --print option to print PDF file. I will share the presentation soon.

> P.S. I am loving the zoom effect.
