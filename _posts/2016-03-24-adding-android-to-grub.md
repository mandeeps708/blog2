---
layout: post
title: "Adding Android-x86 to grub menu"

postday: 2016/03/24
posttime: 21_59
author: Mandeep
---
Approximately one and half years ago I wrote a post about installing Android x86 on PC. You may find here the post: <../../../../2014/09/22/installing-android-on-laptoppc/>

I used [grub-customizer](../../../../2014/09/25/simplifying-boot-menu-with-burg/) to manage my grub. Here is how to add a new entry to show android in the grub menu. Add a new entry.

See more about grub-customizer in the following link.

<../../../../2014/09/25/simplifying-boot-menu-with-burg/>

Add following information as shown in the right hand side of the image.

![android-grub]({{ site.url }}/attachments/android-grub.png)

Just in case, if you are looking to copy paste the source lines then:

**insmod ext4**

**set root='(hd0,8)'**

**search --no-floppy --fs-uuid --set d8c2350c-037b-4e56-8c9f-b52a6807eef7**

**linux /android-4.4-RC1/kernel quiet root=/dev/ram0 androidboot.hardware=android_x86 video=-16 SRC=/android-4.4-RC1**

**drivemap -s (hd0) ${root}**

**chainloader +1**

**initrd /android-4.4-RC1/initrd.img**

 

The lines written above highly depends upon the installation of your android os like partition name etc. that's why I recommended to read the [previous post](../../../../2014/09/22/installing-android-on-laptoppc/) to relate things. Ask if something is unclear.
