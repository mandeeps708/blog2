---
layout: post
title: "Disable Bluetooth on Startup in Ubuntu"

postday: 2015/05/25
posttime: 11_14
author: Mandeep
---
If you don't use wireless mouse or keyboard and disable your bluetooth manually everytime Ubuntu starts up (and hate it), then you need to automate this.

You can do it by editing a file i.e.  /etc/rc.local

You need root permissions to edit this file. From terminal (Ctrl+Alt+T) type:
    
    
    gksu gedit /etc/rc.local

Here gksu is a front-end to su.

Then add the following line before  'exit 0':
    
    
    rfkill block bluetooth

Here is how the file will look like:

![gksu gedit /etc/rc.local]({{ site.url }}/attachments/s.png?w=300)

*gksu gedit /etc/rc.local*

This will soft block the bluetooth service.

You can unblock it anytime by:  rfkill unblock bluetooth.

rfkill is a utility that is meant for disabling and enabling wireless devices.
