---
layout: post
title: "Check file size in human understandable format (KB, MB and GB's)"

postday: 2015/06/15
posttime: 01_38
author: Mandeep
---
Generally we list contents of current directory with the ls command in our terminal. To see more detail we can use ls -l command to see author of file, permissions, size in bytes.

But for humans to easily understand the file size we can list the files with sizes in Kilobytes, Megabytes and Gigabytes (when applied) with:
    
    
    ls -lh
    
    
    
    

But it lists file sizes good but not the directories. So for the directories:
    
    
    du -h .
    
    
    
    

Replace . (dot) with the directory you want.

To see file system disk space usage:
    
    
    df -h
    
    
    
    

 To know more about these commands you can always do man <command_name>.
