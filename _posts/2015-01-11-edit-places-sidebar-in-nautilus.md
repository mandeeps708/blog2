---
layout: post
title: "Edit 'Places' sidebar in Nautilus"

postday: 2015/01/11
posttime: 19_02
author: Mandeep
---
Have you unintentionally deleted any folder from home directory , and now accessing it from the sidebar gives the following error :

![p1]({{ site.url }}/attachments/p1.png?w=300)

Open any text editor you like and provide the non-working path:

vim ~/.config/user-dirs.dirs

![p2]({{ site.url }}/attachments/p2.png?w=300)

There must be a folder named "Pictures" in the home directory to be able to access it from the "Places" menu.

Now Save this file. (:wq)

Execute this command:

nautilus -q

Open nautilus again, then try it.
