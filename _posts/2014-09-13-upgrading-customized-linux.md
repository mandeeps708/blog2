---
layout: post
title: "Upgrading Customized Linux"

postday: 2014/09/13
posttime: 23_21
author: Mandeep
---
If you want to re-install your Linux Distro and afraid of losing your data and customizations done then read on. To take a backup of your system there are several ways. There are many softwares that can be installed to take backup.

 

#### #Aptik

Aptik is a backup software that can be used to save ppa's, software packages (in /var/cache), list the installed packages, Save application settings and themes.

INSTALLATION OF APTIK Open terminal(ctrl+alt+T) and enter the following commands:
    
    
    $ sudo apt-add-repository -y ppa:teejee2008/ppa
    $ sudo apt-get update
    $ sudo apt-get install aptik

MANUAL PROCEDURE: To list all the installed packages on your system, type this command:
    
    
    dpkg --get-selections

([read more about this](http://mandeep7.wordpress.com/2014/09/10/how-to-list-installed-packages)) or Easiest way is : * Open Aptik (By searching in the dash) * Enter your password. * Set your Backup Directory. Choose a directory (e.g. Backedup). * Click on Backup "Software Selections". It will start processing. After sometime, it will create two files named: packages.list and packages-installed.list * The file packages.list is the list of packages that you have installed yourself after installation. * The second file packages-installed.list will list all packages that are currently on the system. It also includes the built-in system packages (like brasero, apt, firefox etc.).

Backup Configuration Files To backup configuration files, select "Backup" along the "Application Settings". It will list configuration files that are in the /home (~) directory. You can tick you want.

Restoration For restoring data in the newly installed Linux, we must install aptik. Then copy the contents of backup (The Backedup directory) from old linux and then paste it in new one. Now open Aptik, select the backup directory(Backedup) that we've recently copied from old linux. Then click "Restore" along "Application Settings".

 

#### #Systemback

Systemback is another backup software that can be used to create a system restore point (something like we do in MS Windows). It also takes backup of user's configuration files.

Installation Open terminal(ctrl+alt+T) and enter the following commands:
    
    
    sudo add-apt-repository ppa:nemh/systemback
    sudo apt-get update
    sudo apt-get install systemback

Features  Besides creating restore points, the tool also allows to:

* Copy system from one partition to another.

* Create new user, set root password, change hostname.

* Create a Live CD/DVD/USB from current system with or without user data.

* System file repair, Grub2 repair.

* Upgrade your Ubuntu to next release

Usage:

Search systemback in the dashboard or open in terminal using command:
    
    
    gksudo systemback

Then in the middle column, there is an option named "Create new" under Point Operations. Click that and it will start creating restore point. Let it complete to 100%. Now, you will see, in the left column a new entry has been made i.e. Restore point. You can manually check this in your /home directory there will be a folder named "Systemback" that is your system restore point. Copy it to your USB (if you want to format current drive).

You can copy it like this:
    
    
    sudo cp -R /home/Systemback /your-destination-path-here

Now re-install linux distro. And Paste the copied "Systemback" folder.
    
    
    sudo cp -R /your-path /home/
    
    
    
    

Then install systemback by following the above steps.

Then open systemback. You will see that there is already an entry of restore point. So check (tick) that entry. Now on the rightmost column, the option "System Restore" will be highlighted. So click that.

You will see a new dialogue, asking to do a Full Restore, User configuration files only etc. So tick user config files option and click next.

Let it complete.

Then you may restart.

 

 
