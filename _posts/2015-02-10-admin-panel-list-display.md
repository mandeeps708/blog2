---
layout: post
title: "Admin Panel List Display"

postday: 2015/02/10
posttime: 09_05
author: Mandeep
---
This is some of part of admin.py file:

class BookingAdmin(admin.ModelAdmin): list_display = ['hall', 'event_name', 'name', 'date', 'start_time', 'end_time', 'email', 'status' ] class Meta: model = Booking admin.site.register(Booking, BookingAdmin)

In this file, there is a new ModelAdmin class named "BookingAdmin". It is connected to the model "Booking" that we created in the models.py file. Hence it did show the name of the hall only previously in the admin panel.

But we need all the details of the booking like name of event, time, date, which hall, organizer details etc. Then it will be easy for the admin to decide which booking to select. So the meaning of second line is this: it displays the fields accordingly as listed in the single quotes.

It will only take effect after we register by writing the last line. It registers the model Booking and module BookingAdmin together so that we can view model's information in the admin panel UI.

Similarly I used the list_display method for Hall model to show the name of the hall and no. of seats when overviewing the list.

 

 
