---
layout: post
title: "Using CDE to port packages"

postday: 2015/06/23
posttime: 23_12
author: Mandeep
---
I came to know about a tool named CDE from my friend [Sukhdeep](https://sukhtaaj.wordpress.com). CDE or CDEpack (Code, Data, and Environment packaging) is a tool, developed by Philip Guo, that automatically packages up everything required to execute a Linux command on another computer without any installation or configuration.

To use it, you have to download it: [32-bit](https://github.com/downloads/pgbovine/CDE/cde_2011-08-15_32bit) or [64-bit.](https://github.com/downloads/pgbovine/CDE/cde_2011-08-15_64bit)

For more visit: http://www.pgbovine.net/cde.html

It is the executable file that runs directly (./cde) after changing permissions (chmod 755 cde).

If we have any software and we want to run it another computer then we can follow something like this: For example I have 'tilda' installed (a terminal app) then I run:
    
    
    ./cde tilda
    
    
    
    

Then it will run tilda application like normal and now you can see the new directory named 'cde-package' created within same directory where cde executable is located. Under cde-package directory, there will be a complete directory structure created like Linux root and all necessary files (dependencies) are added to them that are needed to run tilda application in this case.

Now as per [manual](http://www.pgbovine.net/cde/manual/) on the website, tar and gzip this package directory:
    
    
    tar -cvf cde-tilda.tar cde-package/
    gzip cde-tilda.tar

You can now transfer the `cde-tilda.tar.gz` file to any other x86-Linux distro created in the past 5 years. When the package arrives on the target machine, you can unzip it and then run tilda by executing the special `tilda.cde` wrapper script within the package:
    
    
    tar -zxvf cde-tilda.tar.gz
    cde-package/tilda.cde

But unfortunately this didn't work for me when I sent this to some friend. I'll investigate into the matter.
