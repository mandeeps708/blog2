---
layout: post
title: "Installing Yaourt on ArchBang"

postday: 2015/12/31
posttime: 03_26
author: Mandeep
---
[Yaourt](https://github.com/archlinuxfr/yaourt) (**Y**et **A**n**O**ther **U**ser **R**epository **T**ool) is a wrapper for pacman which adds automated access to the [AUR](https://wiki.archlinux.org/index.php/AUR) using the same syntax as [pacman](https://wiki.archlinux.org/index.php/Pacman).

To put simply, there are some packages that you can't install from the official repositories by the use of default package manager i.e. pacman. So to install packages from Archlinux User Repository (AUR), we need yaourt.

Following is the process of installing yaourt.

Check whether the dependencies (base-devel and yajl) are installed or not:
    
    
    $ pacman -Ss base-devel  # check whether the package is installed
    $ pacman -Ss yajl        # check whether the package is installed
    $ sudo pacman -S base-devel yajl  # install if necessary
    
    
    
    

 

For a cleaner approach create a new directory and work there:
    
    
    $ mkdir -p ~/temp/AUR/ && cd ~/temp/AUR/
    
    
    
    

 

Use the following commands to install package-query:
    
    
    $ wget https://aur.archlinux.org/cgit/aur.git/snapshot/package-query.tar.gz  # download source tarball
    $ tar xfz package-query.tar.gz  # unpack tarball
    $ cd package-query  &&  makepkg  # cd and create package from source
    $ sudo pacman -U package-query*.pkg.tar.xz  # install package - need root privileges
    
    
    
    

 

Now install yaourt in the same way:
    
    
    $ wget https://aur.archlinux.org/cgit/aur.git/snapshot/yaourt.tar.gz
    $ tar xzf yaourt.tar.gz
    $ cd yaourt  &&  makepkg
    $ sudo pacman -U yaourt*.pkg.tar.xz
    
    
    
    

 

Then I tried installing git-cola using yaourt by:
    
    
    yaourt -S git-cola
    
    
    
    

 

Here is my workaround for installing yaourt using above commands. The following is snippet from my history commands (neglect the numbers in the left column):
    
    
     3873  pacman -Ss base-devel
     3874  pacman -Ss yajl
     3875  sudo pacman -S yajl
     3876  mkdir -p ~/temp/AUR/ && cd ~/temp/AUR
     3879  wget https://aur.archlinux.org/cgit/aur.git/snapshot/package-query.tar.gz
     3880  ls
     3881  tar xfz package-query.tar.gz
     3883  cd package-query
     3885  makepkg
     3886  ls
     3887  sudo pacman -U package-query*.pkg.tar.xz
     3888  cd ..
     3889  wget https://aur.archlinux.org/cgit/aur.git/snapshot/yaourt.tar.gz
     3892  ls
     3902  tar xzf yaourt.tar.gz
     3904  cd yaourt && makepkg
     3905  sudo pacman -U yaourt-1.7-1-any.pkg.tar.xz
     3907  yaourt
     3908  yaourt -S git-cola               #just to test if it works
    
    
    
    

**Reference**: https://astrofloyd.wordpress.com/2015/01/17/installing-yaourt-on-arch-linux/
