---
layout: post
title: "Using Qt UI files with PySide in FreeCAD"

postday: 2017/05/07
posttime: 22_53
author: Mandeep
---
In Qt Designer app, create an interface. For example, I firstly created a form with two text widgets and then one push button.

![qt ui design]({{ site.url }}/attachments/qt_ui.png?w=800)

Paste the following in the FreeCAD Python console:

[code language="python"] from PySide import QtCore, QtGui

class CalculateSum: def __init__(self): # Importing the form from Qt UI file. self.form = FreeCADGui.PySideUic.loadUi("/home/mandeep/ui-design/inputform.ui")

### Setting on-click behavior of Calculate button QtCore.QObject.connect(self.form.calculateButton, QtCore.SIGNAL("clicked()"), self.accept)

def accept(self): """Will be called when Calculate button is clicked""" # Take inputs from both fields. v1 = int(self.form.val1.text()) v2 = int(self.form.val2.text()) print("Sum of "+str(v1)+" and "+str(v2)+": "+str(v1+v2)) # Show up in FreeCAD. if FreeCAD.GuiUp: FreeCADGui.Control.showDialog(CalculateSum()) [/code]

You will see the widget (Form) inputform.ui in the Combo View Tasks panel in FreeCAD. Enter values and click the Calculate button and the result will be printed in the Report View.

![2017-05-08-001246_1366x768_scrot.png]({{ site.url }}/attachments/2017-05-08-001246_1366x768_scrot.png)
