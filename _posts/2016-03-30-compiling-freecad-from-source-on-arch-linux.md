---
layout: post
title: "Compiling FreeCAD from source on Arch Linux"

postday: 2016/03/30
posttime: 04_21
author: Mandeep
---
**Getting the source**:

git clone https://github.com/FreeCAD/FreeCAD free-cad-code

**Getting the dependencies**:

You will need the following libraries from the official repositories:

Just use:

_sudo pacman -Syu  boost-libs curl hicolor-icon-theme libspnav opencascade python2-pivy python2-matplotlib python2-pyside python2-shiboken qtwebkit shared-mime-info xerces-c boost cmake coin desktop-file-utils eigen gcc-fortran swig xerces-c_

And one with [yaourt](../../../../2015/12/31/installing-yaourt-on-archbang/):

_yaourt -S python2-pyside-tools_

**Compiling:**

_cd free-cad-code_ (the folder where you cloned the freecad source)

For a Release build:

$ _cmake -DFREECAD_USE_EXTERNAL_PIVY=1 -DCMAKE_BUILD_TYPE=Release ._ $ _make -j4_

**Execution:**

Execute using the following command:

_./bin/FreeCAD_

 

**Source: **<http://www.freecadweb.org/wiki/?title=CompileOnUnix>
