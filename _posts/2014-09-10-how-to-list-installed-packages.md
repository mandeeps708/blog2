---
layout: post
title: "How to List installed Packages"

postday: 2014/09/10
posttime: 21_05
author: Mandeep
---
To list installed packages in Ubuntu or other debian based systems:
    
    
    dpkg --get-selections

To copy this list to a file, type:
    
    
    dpkg --get-selections > file.txt

To list installed packages in Fedora or rpm based linux, type:
    
    
    rpm --queryformat="%{NAME}-%{VERSION} \n" -qa
