---
layout: post
title: "Finding intersection point"

postday: 2015/11/06
posttime: 11_54
author: Mandeep
---
Now the intersection points have been calculated by solving two equations simultaneously.

<https://github.com/mandeeps708/PyDrain/blob/master/dxf_backup.py>

Now just have to add the logic for calculating intersection point by using which one of the line out of two.
