---
layout: post
title: "Copy to system clipboard in VIM"

postday: 2015/10/10
posttime: 23_59
author: Mandeep
---
You might have noticed that when you copy something from vim and try to paste somewhere else (maybe your browser), then it doesn't work.

First of all you need to install the following packages:
    
    
    sudo apt-get install vim-gnome
    
    
    
    

Most probably, vim-gui-common package will get automatically installed with it.

Now, you may confirm it using:
    
    
    vim --version | grep 'clipboard'
    
    
    
    

If it says +clipboard then you are good to go. At present, my vim version is 7.4.52.

Now open any file with vim, and select text using v or V. Then press "+y to copy it to system clipboard. And "+p to paste in another vim file.

I found this helpful on vim wikia:

![vim wikia]({{ site.url }}/attachments/photo_2015-10-10_23-46-21.jpg)
