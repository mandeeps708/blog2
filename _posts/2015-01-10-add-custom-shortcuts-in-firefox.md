---
layout: post
title: "Add Custom Shortcuts in firefox"

postday: 2015/01/10
posttime: 01_35
author: Mandeep
---
First of all, you have to download firefox addon 'Customizable Shortcuts'. You can do this by searching it on google or in add on manager. Then click on Add to Firefox.

![plugin customizable shortcuts]({{ site.url }}/attachments/1.png?w=300)

Now let it download. After downloading click on 'Allow' Popup.

Then click on Install Now.

![firefox addon install]({{ site.url }}/attachments/2.png?w=300)

*firefox addon install*

Now open preferences by going to Edit > Preferences and now click on Shortcuts and then choose any action and double click on relative Shortcut tab and edit it using any unique combination.

![shorcut preference in firefox]({{ site.url }}/attachments/3.png?w=300)
