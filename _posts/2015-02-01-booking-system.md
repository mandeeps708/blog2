---
layout: post
title: "Booking System"

postday: 2015/02/01
posttime: 01_51
author: Mandeep
---
Me and [Raman ](http://ramanvirdiz.wordpress.com)decided some functionality of the software and hence we created a rough sketch of what will be the work flow. And later I made a simulation of the software in the form of a animation:

![seminar-final]({{ site.url }}/attachments/seminar-final.gif?w=300)

Hence it will have three modules on the home page(as of now):

  1. Click to book -> This module will have a form in which user will input the data regarding hall, event and himself and then proceed.

  2. View Bookings -> This module will contain all bookings that will be accepted by the admin to be booked.

  3. Cancel Bookings -> This module will include the all the bookings that a user can cancel if he/she wants to do so. We will think of it as a base design(structure) the software and start preparing the software soon.
