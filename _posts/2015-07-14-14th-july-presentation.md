---
layout: post
title: "14th July - Presentation"

postday: 2015/07/14
posttime: 23_41
author: Mandeep
---
Today I gave presentation on  [Reveal-md](../../../../2015/07/12/exploring-reveal-md/). The code is at <https://github.com/mandeeps708/reveal-md-presentation>

The main presentation file is [demo/presentation.md](https://github.com/mandeeps708/reveal-md-presentation/blob/master/demo/presentation.md). I got some problems while overriding the data-background-transition for a particular slide when there are defaults defined (for all slides) in reveal.json. I'll look into it later. 

I used '[Home Remote Control](https://play.google.com/store/apps/details?id=com.inspiredandroid.linuxcontrolcenter&hl=en)' android application to control the presentation from my mobile. I have an older version of this application. We need to have installed xdotool (sudo apt-get install xdotool) and ssh on linux to use all the features.

Also there were presentations on IRC and make command.

I tested the projector's speakers today by playing a video in the presentation itself (added a video [offline] to slides). Normally it don't work. I used 'PulseAudio Volume Control' to test the sound. BTW projector have good speakers.
