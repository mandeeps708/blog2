---
layout: post
title: "Beautify Readme.md on Github"

postday: 2015/03/07
posttime: 01_05
author: Mandeep
---
Today I was updating the README.md file of the repository on Github and then I thought to make it look good. So I searched about it. After exploring I came to know that Github uses a language called 'Markdown' for these purpose and I browsed about how to use it. After getting the basic syntax, I started to write it in a website: http://tmpvar.com/markdown.html which gives live preview as we type markdown. You can see it in the image attached.

![markdown live preview]({{ site.url }}/attachments/tmpvar-com.png?w=300)

*Markdown Live Previw*

You can see the live preview of the README.md at the github repository: http://github.com/rvirdiz/booking_system
