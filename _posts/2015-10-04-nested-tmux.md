---
layout: post
title: "Nested Tmux"

postday: 2015/10/04
posttime: 01_56
author: Mandeep
---
If you don't know the basic functionality of tmux, then visit [this](../../../../2015/07/24/tmux/).

Now, we know that we can create different sessions using tmux. But what if we want the sessions open concurrently on the same window. After opening tmux, we can divide the window in several sessions. To execute things in tmux, we first have to send command prefix first and then press some key. For example, first type Ctrl-b and then press %, to divide the window into two vertical panes. You can do the similar if you want to do it horizontally. Type Ctrl-b and then " (double quotes or shift+'). You can use different combination of them. To cycle between all the panes, press Ctrl-b and then o.

Now if you want to nest one tmux session into existing opened tmux session, you would type tmux again! But this doesn't work simply. You'll probably got something similar: "_sessions should be nested with care, unset $TMUX to force_". To overcome this, you may use shortcut Ctrl-b and then type :new and press 'Enter'. This will open a new session window. But as they warned, sessions should be nested with care. :) You can always use tmux list-sessions to know list of currently opened sessions and you can connect to them using:
    
    
    tmux attach -t 2
    
    
    
    

Here 2 is the id of the session, that you'll get from the list-session command.

Another easy way to switch between the sessions is to press Ctrl-b and then press s. Then browse the sessions and press 'Enter' to get attached to it.

Now I had a problem while nesting tmux sessions. Suppose you have tmux running and then you ssh into a remote machine and run tmux there. Then, while detaching the tmux session on the remote machine, your local tmux will be detached with the normal Ctrl-b and d.

So, to overcome this, you have to send two command prefixes i.e. Ctrl-b (one for the local tmux and another for talking to the remote machine's tmux). Hence type: Ctrl-b Ctrl-b and then d to detach the tmux on ssh one.

 

 

 
