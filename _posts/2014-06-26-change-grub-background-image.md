---
layout: post
title: "Change GRUB Background image"

postday: 2014/06/26
posttime: 11_05
author: Mandeep
---
GRUB is abbreviation of GNU GRand Unified Bootloader. GRUB is a program by GNU that let the user choose the operating system if there are multiple operating systems installed on a single system. So if you have installed ubuntu or other distribution of linux then you must have seen the GRUB screen. On ubuntu, usually it is a purple colored screen. If you got bored from that simple, old purple background or you want to impress your friends, then read on. So in this post, I am telling you about changing the background image of GRUB.

> **#STEP 1**

Its better to paste the image first in this folder using terminal:
    
    
    sudo su
    
    
    cd /usr/share
    
    
    mkdir image
    
    
    cp <---your image path like /home/mandeep/Desktop/ubuntu.jpg---> image/ubuntu.jpg

![1](http://mandeep7.files.wordpress.com/2014/06/1.png?w=300)

You can see the above screenshot. In this I have image in my /usr/share/image folder and the image is ubuntu.jpg . Firstly, my image ubuntu.jpg was in /home/mandeep/Desktop folder. Then I copied it to image folder in /usr/share.

 

> **#STEP 2 (Using Command-line)**

In terminal type:
    
    
    sudo su

Then goto the directory:
    
    
    cd /etc/default

Then open the file "grub" using any editor. You may use vim, nano, gedit. So just change nano by any other:
    
    
    nano grub

Then type the following command with absolute path of your image:
    
    
    GRUB_BACKGROUND="/usr/share/image/ubuntu.jpg

See my grub file screenshot:

![2](http://mandeep7.files.wordpress.com/2014/06/2.png?w=300)

 

> **#STEP 3 **

Then save the file.

In terminal type following command to update GRUB for the changes to take place:
    
    
    update-grub

![3](http://mandeep7.files.wordpress.com/2014/06/3.png?w=300)You will see the above output.

You can reboot now and skip next steps if you don't want to change the text colors.

> **#To change the text color and highlight color of GRUB**

In terminal type:
    
    
    gksu gedit /etc/grub.d/05_debian_theme

Then locate:
    
    
    if [ -z "${2}" ] && [ -z "${3}" ]; then
                    echo "  true"
            fi

Or just type Ctrl +f to find it write in it : if [ -z or simply 'z'. Then replace the above paragraph with this one:
    
    
    if [ -z "${2}" ] && [ -z "${3}" ]; then
            echo "  true"
            echo "  set color_highlight=red/green"
            echo "  set color_normal=light-cyan/black"
        fi

Then save the file. In terminal type following command to update GRUB for the changes to take place:
    
    
    update-grub

Then reboot the system.

![grub background](http://cdn.imghack.se/images/de3fe19485e052ad1263b741fb8ec157.png)

Change the color accordingly or choose from the below list:
    
    
    black, blue, brown, cyan, dark-gray, green, light-cyan, light-blue, light-green, light-gray, light-magenta, light-red, magenta, red, white, yellow
