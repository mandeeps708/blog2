---
layout: post
title: "6th June"

postday: 2014/06/06
posttime: 14_50
author: Mandeep
---
Today I came to college earlier in the morning. I installed the Ubuntu 14.04 on my new laptop. After installing it I also have to reinstall all the packages that I have installed on the old one. So today I did less amount of project work. Today I have cloned the LibreCAD and C++ version 11 programs. And I have compiled the LibreCAD version 3 using QT creator. Hence learned some of the basic techniques of compiling. First there was an error occurring after installing QT that it was not getting executed after installation. Then I reinstalled it and executed it after checking for dependencies. It's good to see it working.

Today there was a presentation of 3D printing by Satyam and it was great to listen to him. He had shared his experience with us that how 3D printing actually works. It was a very interesting topic.
