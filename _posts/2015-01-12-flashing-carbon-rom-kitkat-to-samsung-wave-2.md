---
layout: post
title: "Flashing Carbon ROM (Kitkat) to Samsung Wave 2"

postday: 2015/01/12
posttime: 00_00
author: Mandeep
---
Yesterday, my Samsung Wave 2 (8530) having Bada OS stopped working and keep on restarting suddenly. Then I searched over Internet about flashing new ROM to it and found some results. The best of them that worked for me is: http://forum.xda-developers.com/samsung-tizen/bada-android/rom-carbonrom-s8500-s8530-t2840174

###### What is Carbon ROM?

![carbon ROM](http://i.hizliresim.com/AZnnNv.png)

*Carbon ROM*

Carbon ROM is the project that incorporated the code of CyanogenMod. Carbon Rom is an aftermarket firmware based on the Android Open Source Project. We are dedicated to fast, stable, and feature-filled roms, honesty and communication with our users, and openness with our code. We like frequent builds, with the very latest and greatest hardware support and fixes. We strive to not only provide you with the best rom we can build, but also to give back to the Android community and our fellow developers. For us, this is about creating something we can be proud of and hope you will enjoy.

###### Download:

  1. [Download Carbon ROM](https://yadi.sk/d/NyvvqEpwZF7KF) OR [Mirror](https://mega.co.nz/#!kE52iJzK!8GSgNh9yx2DPSq371_GjD5mj11tWP0Zol_bJF5oHsRc) (185 mb)
  2. [*Normal Kernel - Download (283)* ](https://mega.co.nz/#%21AB423LKI%21hT9hviks7HXsx8WW0nUeLT28rhUBzK7U6LVigugtxBA)OR [(Mirror)](https://yadi.sk/d/XXk33PYsZF7Uo)       AND  You may also try these kernels: [Kernel - Download (310)](https://mega.co.nz/#%21BABHSBhY%21sI0gnOwIXI_x4XMvq8-TkTqlg_k3G13aHBFKo8QkhwE) AND [*Bigmem Kernel](https://mega.co.nz/#%21JQAyQKDK%21CDngTy770PB9XM_Yzs3ez3Ns0VYg9pFYzIBM-YAEhik)[ \- Download (314)* ](https://mega.co.nz/#%21JQAyQKDK%21CDngTy770PB9XM_Yzs3ez3Ns0VYg9pFYzIBM-YAEhik)OR [(Mirror)     ](https://yadi.sk/d/WD00nwUDZExSu)(6 mb)
  3.  Download FOTA for 8530 wave 2: [FOTA ](https://mega.co.nz/#%21xURXAAYL%21bp4NiQyj1Sh33OYJMJ_yCtBBhXFzGLuHG4gBSsG2tg0) (2.5 mb)
  4.  Multiloader (.exe file)  [Download Here (6 mb)](http://www.nextwap.net/file/SLagBW72/multiloader_v565_.html)
  5. Samsung USB Drivers. Download from [Mediafire Link (15 mb)](http://www.mediafire.com/download/a4hd8y0c1iakysk/Samsung-Usb-Driver-v1.5.49.0.exe)

###### Requirements:

  1. Samsung Wave2 (8530)
  2. SD Card is required. (fat32 format!!!)

Procedure:

1\. First of all copy these files to the SD Card (by putting it in another phone if your wave 2 is not working):

  * The .zip file (ROM downloaded in step1, size about 185mb)
  * boot.img (Kernel downloaded in step2)(Rename the kernel file as boot.img)

2\. Now Put your Memory card loaded with above two files into wave 2 and Press the 3 keys simultaneously:

Volume Down + Lock Screen + End Call

Now a different screen will be appear on screen saying: "Download Menu".

4.  Now Attach your phone with the PC using USB cable(also called Data Cable).

5\. Open Multiloader on Windows PC (Win 7).

6\. Click on Port Search. After pressing there will be message appear: "Wave 2 Ready".

7\. Then click on LSI (if not selected) and Click on FOTA.

8\. It will open a prompt menu to select FOTA file and most probably it will appear automatically to you (if not locate .fota file downloaded in step3)

9\. Then at the end Click on Download Button. It will take 2-3 seconds.

10\. Now the phone will Restart itself. Now Disconnect the USB cable from Phone. You will see Android Recovery Menu now.

11\. * Install -> choose zip with ROM -> Swipe to Confirm Flash

It will take some time. After that click on HOME button.

12\. Now follow this :    ** Wipe -> Advanced wipe -> Check "data" -> Repair or change File Sytem -> Change File System -> Ext4 -> Swipe to change

13\. Now Back -> Back >Back -> Back (4 or 5 times) until Reboot button appears.

14\. ** Reboot -> System

 

Now the system will get restarted. If it works for you then enjoy and if you are getting Encryption Problem saying 'Reset Phone' then remove battery and reinsert it and Press these 3 keys together:

Volume UP + End Call + Lock screen

This will show a white screen and then some option will be appeared. Then try changing your boot image to normal android image.

Thanks to my brother whose hands did the magic of crashing my phone's BADA OS and I came to learn about these things. :)

Extra Info:

**New Fota settings: End = Android Normal End + VolDown = Bigmem End + Volup = Recovery End + Call = Bada offline charge**

P.S: When you press buttons for booting any mode, after yo see white screen, stop pressing.

You can remove installation .zip from SD card from Android level by connecting through USB in MTP mode.

Do not remove boot.img file.

For activation Performance and Developer Options categories in Settings you need Settings->About Phone and tap 7 times on Build number

Reference: [xda-developers](http://forum.xda-developers.com/samsung-tizen/bada-android/rom-carbonrom-s8500-s8530-t2840174)

There are another ROMS available for wave 2, you can try this too: http://forum.xda-developers.com/showthread.php?t=2306458
