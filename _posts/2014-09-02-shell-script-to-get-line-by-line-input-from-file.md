---
layout: post
title: "Shell Script to Get line by line input from file"

postday: 2014/09/02
posttime: 00_03
author: Mandeep
---
It is the shell script:
    
    
    #!/bin/sh
    while read line
    do
    echo $line
    done <filename

Here **filename** is the name of file to be given as input to the script and "line" is a variable. The "read" keyword can be used to read a line from file. To execute this file successfully, you need to have a file named "filename" in the same folder having some text written in it.

**Note:** Change the permissions of this shell script.
