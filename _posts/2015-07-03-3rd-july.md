---
layout: post
title: "3rd July"

postday: 2015/07/03
posttime: 21_36
author: Mandeep
---
Today I tried to modify the same functions again. I added a option for user to enter threshold value manually if he/she wishes to. The user may optionally enter threshold value or the default value is set to 0.45.

I used separate function manmorph.m to use iopen and iclose operations of the toolbox.
    
    
    function out = manmorph(image)
    ip = iopen(image, ones(20,20)); %erosion then dilation
    %out=image;
    out = iclose(ip, ones(20,20));  %dilation then erosion
    
    
    
    
