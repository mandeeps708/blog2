---
layout: post
title: "Knowledge Sharing Session"

postday: 2014/07/06
posttime: 22_30
author: Mandeep
---
There was a knowledge sharing session till yesterday in the TCC seminar hall. As the name suggests, it was about giving seminars and presentations and sharing knowledge with all. I came to know about [Aaron Swartz.](http://en.wikipedia.org/wiki/Aaron_Swartz) Such a great Being he was. At the age of 14, he co-authored RSS. He fought for making web content [free](http://harvardmagazine.com/2013/01/rss-creator-aaron-swartz-dead-at-26) and open to people. Another group from college also took part in this session. They are working on Big-data. Nice seminars were presented. Learned something new.
