---
layout: post
title: "Enable Num Lock on Ubuntu Login screen (lightdm)"

postday: 2014/11/21
posttime: 23_43
author: Mandeep
---
In this article, I am going to tell about how to enable num lock while typing password on login screen. Every time, you start Ubuntu 14.04 you have to press num lock button on keyboard (if you have a numeric password).

##### What is Numlockx:

numlockx is a program to control the NumLock key inside X11 session scripts (login screen).

####### OPTIONS

  * on: Turn NumLock on
  * off: Turn NumLock off
  * toggle: Toggle NumLock to opposite status
  * status: Gets the NumLock status

Try these on terminal by giving these as parameter to numlockx command like "numlockx status".

First of all you need to install numlockx:

###### STEPS:

1\. Open terminal(Ctrl+Alt+T).

2\. Type command:
    
    
    sudo apt-get install numlockx

 

Now to run numlockx on Login screen, you need to edit lightdm configuration file. To do that:

open conf file with your text editor with sudo privileges by:

####### _(you can use any text editor you want. Just replace its name with vim)_
    
    
    sudo vim /usr/share/lightdm/lightdm.conf.d/50-unity-greeter.conf

Add the following line to this file:
    
    
    greeter-setup-script=/usr/bin/numlockx on

Then save the file.

Next time when you'll start ubuntu, your num lock will be automatically enabled to type password.
