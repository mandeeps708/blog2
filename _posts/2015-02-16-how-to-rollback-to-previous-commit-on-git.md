---
layout: post
title: "How to rollback to previous commit on Git?"

postday: 2015/02/16
posttime: 01_02
author: Mandeep
---
First of all, you need to check the commit id of the previous changed state. You can do it using command:
    
    
    git log
    
    
    
    

or you can also modify to ease in finding id like:
    
    
    git log --since=1.weeks
    
    
    
    

Now copy the commit id. Now execute this command:
    
    
    git reset --hard commit-id
    
    
    
    
