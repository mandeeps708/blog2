---
layout: post
title: "Installing RAINLOOP"

postday: 2015/03/28
posttime: 17_43
author: Mandeep
---
RAINLOOP is Simple, modern & fast web-based email client. An **email client**, **email reader** or more formally **mail user agent** (MUA) is a computer program used to access and manage a user's email.

#### Installation

![download rainloop]({{ site.url }}/attachments/download-web.png?w=300)

Create a new directory named rainloop:
    
    
    mkdir /var/www/rainloop
    
    
    
    

 

Extract the downloaded zip file:
    
    
    unzip rainloop-latest.zip -d /var/www/rainloop
    
    
    
    

 

Grant read/write permissions required by the application:
    
    
    cd /var/www/rainloop
    find . -type d -exec chmod 755 {} \;
    find . -type f -exec chmod 644 {} \;
    
    
    
    

 

Set owner for the application recursively:
    
    
    cd /var/www/rainloop
    chown -R www-data:www-data .
    
    
    
    

 

You'll need to create database and do some configuration settings. Please check the official [rainloop](http://rainloop.net) website for more details.

Now go to the browser and enter localhost/rainloop and you'll be greeted with rainloop authentication.

Now if you have 2 step verification activated on Gmail then you can go to the page: <https://security.google.com/settings/security/apppasswords>

and then create a new authentication code for your application. Copy this code and paste it in the password field of rainloop and in enter your email too.

The view of settings:

![rainloop settings]({{ site.url }}/attachments/rainloop-settings.png?w=300)

 
