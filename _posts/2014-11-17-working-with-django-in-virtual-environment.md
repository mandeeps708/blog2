---
layout: post
title: "Working with Django in virtual environment"

postday: 2014/11/17
posttime: 20_12
author: Mandeep
---
## Installations:

You need to install:

_1\. pip_

_ 2\. virtualenv_

  * To install pip, type in terminal:
    
    
    sudo apt-get install python-pip

  * Install virtualenv using pip. Type in terminal:
    
    
    pip install virtualenv

 

#### Configuring Virtualenv:

  * After installing virtual environment (virtualenv), in the current directory type:
    
    
    virtualenv venv

  * Then activate the virtualenv by typing:
    
    
    source venv/bin/activate

  * After this command, you'll see "(venv)" in your terminal before your username.

![activating virtualenv image]({{ site.url }}/attachments/venv.png?w=300)

*activate virtualenv*

#### Installing Django in virtualenv

  * Now install django with pip:
    
    
    pip install django

  * Now in the venv/bin folder, you'll see some files regarding django. It means Django is installed in your virtual enviroment.

Now visit https://docs.djangoproject.com/en/1.7/ for official documentation and follow the tutorials.
