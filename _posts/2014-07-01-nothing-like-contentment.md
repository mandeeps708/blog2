---
layout: post
title: "Nothing like Contentment"

postday: 2014/07/01
posttime: 11_59
author: Mandeep
---
Now one month of training is over. We are not finding any sufficient algorithm on Internet to study. So today, we discussed about the problem with Rai sir, about implementing a new hatching algorithm. But sir told us to use present libraries. He also added that there is no need to create new libraries when we can use existing as it would lead to totally wastage of time. He gave some reasonable examples. So today we searched on Internet about different methods to find contour of an object. Because for implementing hatching, we need an closed contour object. Contour is basically a boundary covering a particular region. I came to know about a new **OpenCV** library which is the Open source Computer Vision Library. It's written in C and C++ with over 2500 functions.  It officially supports Linux, Mac OS, Windows, Android and iOS. OpenCV can be downloaded from here according to your need from this link : http://opencv.org/downloads.html I found a link of a program that uses it : http://stackoverflow.com/questions/8449378/finding-contours-in-opencv .
