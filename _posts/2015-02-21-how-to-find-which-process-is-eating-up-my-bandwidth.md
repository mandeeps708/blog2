---
layout: post
title: "How to find which process is eating up my bandwidth?"

postday: 2015/02/21
posttime: 10_51
author: Mandeep
---
Nethogs is the tool to monitor the processes that are using internet and it also specifies how much bandwidth (speed in KB/sec) i.e. how much data is sent and how much is received.

##### Installation:

Install nethogs using command:
    
    
    sudo apt-get install nethogs

If you are using wireless connection, then specify the device like:
    
    
    sudo nethogs wlan0

I found the process "java" was eating up bandwidth and I killed the process using command:
    
    
    killall java

Then you may again execute nethogs command to see if it's gone or not.
