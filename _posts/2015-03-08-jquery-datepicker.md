---
layout: post
title: "Jquery Datepicker"

postday: 2015/03/08
posttime: 01_19
author: Mandeep
---
For using jquery datepicker in a django form, I included four external files:
    
    
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    
    
    
    
    
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    
    
    
    
    
    <script src="http://code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
    
    
    
    
    
    <link rel="stylesheet" href="http://jqueryui.com/resources/demos/style.css">
    
    
    
    

in the base.html and then added a script
    
    
    <script>
    $(function() {
    $( "#id_date" ).datepicker();
    });
    </script>
    <script>
    $(function() {
    $( "#id_hall" ).selectmenu()
    .selectmenu( "menuWidget" )
    .addClass( "overflow" );
    });
    </script>
    
    
    
    

So on viewing the book.html page (that contains the form), I viewed its source and found id of the date input field(#id_date) and then assigned it accordingly in the script above. In the script, I also added input id of hall(#id_hall) to show it as a nice looking dropdown list.

Also added some styles:
    
    
    <style>
    fieldset {
    border: 0;
    }
    label, label#id_start_time{
    display: block;
    margin: 10px 0 0 0;
    }
    select {
    width: 200px;
    
    
    
    
    
    }
    .overflow {
    height: auto;
    }
    </style>
    
    
    
    

The commit can be seen @ https://github.com/rvirdiz/booking_system/commit/c9e65898170af181915f1e53952c5a1037ca01cf I'd also downloaded the jquery files and css in the static folder but it didn't work may be because of some path error. 
