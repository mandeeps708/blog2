---
layout: post
title: "Vim tweaks"

postday: 2015/10/14
posttime: 11_20
author: Mandeep
---
**Get vim awesome vimrc**

You can find the vimrc on GitHub from many repositories. I found <https://github.com/amix/vimrc> and I just gave it a try. You can read the Readme file for more on the installation of vimrc.

.vimrc is a file that is stored in the home directory (~/) usually and it stores the configuration settings related to Vim.

* * *

**Note**: You should remain in the command mode while performing selections etc. operations below. Be sure, by pressing the 'Esc' button (default).

Some Vim commands that I learnt while experimenting are discussed below.

  * **Indent blocks**:

First of all, select block of lines using v. Press v to start selecting and then press j to go down while selecting lines. Or simply press V to select whole lines and then press >> to indent the whole selected block the right and << to do the same to the left. Then press dot to repeat the same procedure (i.e. indent left or right). You may also do it for a single line.

You can also visually select the blocks using Ctrl-v. Then press jj and ll repeatedly to select a block of text.

This may be helpful while performing any operation on a selected text or block.

First select a visual block using above methods.

Then press the : _(colon key or shift+;) _

You might notice something already written there when you press the 'colon' key. Don't press Enter or selection will be lost.

Then you can perform operations like search and replace by pressing s/text-to-search/to-be-replaced-with/ immediately after pressing the colon.

  * gg to move to the top of the file.
  * 0 to move to the beginning of the line.
  * Just press any number and then the command to repeat that command the number of times. e.g. press 7 and then j. We know that pressing j will let us go down by one line. But 7 j will do the j, 7 times i.e. move to 7 lines down.
  * To _insert_ some text in the _beginning of each line_ or some line:                                                                                    select the lines using v or V buttons and then press colon and then type s/^/% /

To add % in the beginning to each of selected lines. To remove them, you can do something like the following after making selection:

s/^%/ /

This will replace it with space.

You may like to read my post: [Copy text to system clipboard in VIM](../../../../2015/10/10/copy-to-system-clipboard-in-vim/).
