---
layout: post
title: "BeautifulSoup"

postday: 2016/06/06
posttime: 06_19
author: Mandeep
---
FreeCAD manages its macros its wiki: <http://www.freecadweb.org/wiki/index.php?title=Macros_recipes>. Let's take an example of a macro "Macro Image Scaling" whose code is at <http://www.freecadweb.org/wiki/index.php?title=Macro_Image_Scaling>. I needed to fetch the code from wiki and further process that.

Lately, I was doing some experiments to parse macro code from the FreeCAD wiki. Firstly, I used [PyQuery](https://github.com/mandeeps708/scripts/blob/master/FC_getMacro/fcmacro.py). But it uses lxml which is a shared library and it was having some issues while packaging for Windows. As lxml is a shared lib and the available Python 2.7 lxml packages were built using VC9 (Visual C++) which isn't compatible with FreeCAD's python which is built with VC12. FreeCAD ships its own Python on Windows.

Then another day I found BeautifulSoup via [Daman](https://fumblehool.wordpress.com/2016/06/05/5th-june-2016/). It's pretty good. You may see the use case here: <https://github.com/mandeeps708/scripts/blob/master/FC_getMacro/fcmacrobs.py>.

But while running this as macro in FreeCAD, the output gets printed to the report view. But after executing this macro, the report view kept on stopping at a particular character of fetched macro output code. This seemed to be some problem with length of string redirected to the report view. The code works fine in Python console.

 
