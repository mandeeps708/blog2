---
layout: post
title: "Record Audio from terminal"

postday: 2014/11/24
posttime: 01_51
author: Mandeep
---
If you want to record your voice in your system, then read on. If you have a notebook or laptop, then most probably there will be a built-in mic present in it. If you're using PC then you need to attach mic. I am currently using Ubuntu 14.04. To check mic (audio input):

###### System Settings

  * Go to System Settings ▸ Hardware ▸ Sound (or click on the speaker icon on the menu bar) and select Sound Settings
  * Select the Input tab
  * Select the appropriate device in Select sound from
  * Make sure the device is not set to Mute.
  * You should see an active input level as you use your device

![audio input image]({{ site.url }}/attachments/input-check-mic.png?w=300)

*System settings*

Open your terminal (Ctrl+Alt+T) and type:
    
    
    arecord test.wav

It will now start recording your voice. Now talk what you want and after that press ctrl+c to close when you are done. There will be a file created test.wav in your current directory. Open it using rhythmbox:
    
    
    rhythmbox test.wav

You can use any name of the file (test.wav) just replace it by any name.
