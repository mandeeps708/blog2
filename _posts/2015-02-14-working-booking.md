---
layout: post
title: "Working Booking"

postday: 2015/02/14
posttime: 08_18
author: Mandeep
---
As Raman started working on User Authentication, I decided to work on the Bookings. In home/forms.py I created a BookingForm class which was linked to the 'Booking' model using model = Booking

As you can see in the commit: https://github.com/rvirdiz/booking_system/commit/fc48cd1cc9b41ff953bd265236a040f4cfc3f848

In the class meta: , there are fields specified which will be shown on the html for the input. This input will then be stored in the database. So we call this form in the book.html page using {{ form.as_p }} and then in the views.py, I called the template book.html which contains the form (BookingForm). There is a validation applied that if form is valid only then save it to the database else show the errors.
