---
layout: post
title: "Script to set background to reveal slides"

postday: 2015/07/17
posttime: 11_51
author: Mandeep
---
17th July

Sir told me to make a script to create slides just by putting some images into a folder. Nice idea!

After trying some commands interactively, I wrapped the commands and first tried for manipulating text of a file. I got opportunity to use the sed (stream editor). Here is the script for that: [Click here to see the script](https://github.com/mandeeps708/scripts/blob/master/presentation/presentation.sh).
    
    
    let count=1
    while read line
    do
    sed -i -e s/'variable('$count')'/$line/ filename
    ((count++))
    done<list

Here [list](https://github.com/mandeeps708/scripts/blob/master/presentation/) contains the list of names of images that are to be replaced with the 'variable()' in the file 'filename'.

Then I got the idea and created another script to add slides into the [reveal-md presentation](../../../../2015/07/14/14th-july-presentation/) file.

Here is the link to script: [addslide.sh](https://github.com/mandeeps708/scripts/blob/master/presentation/addslide.sh)
    
    
    #!/bin/bash
    
    
    
    
    
    while read line
    do
    echo "">> presentation.md
    echo "---">>presentation.md
    echo "">>presentation.md
    echo "<section data-background="images/$line"></section>">>presentation.md 
    done<list

The lines I've echo'ed are the syntax to add a new slide in reveal-md presentation (three hyphens surrounded by new lines). Basically a new line is fetched from the file "list" (which contains the list of images in the images folder) and then it's appended (due to '>>') in the file [presentation.md](../../../../2015/07/14/14th-july-presentation/) by inserting the image names (from file 'list') within section tags (to change background).

After that I pushed the code to [github](https://github.com/mandeeps708/scripts/tree/master/presentation).
