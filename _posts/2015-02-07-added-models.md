---
layout: post
title: "Added Models"

postday: 2015/02/07
posttime: 11_33
author: Mandeep
---
After watching the [animation](../../../../2015/02/01/booking-system/) a few times and discussing about it with others, we started created project named "bookingsystem" and then an app named "home".

In home, there is a models.py file, in which we created two models: Booking and Hall.

The model 'Booking' is having these fields:

  * hall (a foreign key to 'Hall' model)- This field is for the location where event is to be held.
  * date \- It is for the date on which the booking is to be done.
  * start_time \- The start time for the event to be held at the hall.
  * end_time \- The end time of the event.
  * name \- Name of the organizer
  * email \- Email id of organizer
  * event_name \- Name of the event to be held.
  * status \- Status of the booking(To be decided by the admin).

The 'Hall' model has two fields (as of now):

  * hall \- name of the hall
  * seats \- number of seats available in the hall
