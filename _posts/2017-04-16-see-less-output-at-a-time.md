---
layout: post
title: "See 'less' output at a time"

postday: 2017/04/16
posttime: 19_27
author: Mandeep
---
There are a lot commands/tools that produce a lot of output that we can't see at once (and without scrolling). Most of the commands on terminal can be made to show us whole output page by page using "less" command.

> **less** command_here
> 
> **less** ls -R ~/

or you may pipe the output to less command,

> ls -R ~/ **| less**

 

or similarly, to see the contents of a file:

> cat file_name | **less**

There are many commands/tools that have their own prompt.

 

For **IPython**, you may do:

> **%page** variable_name

 

Similarly for **MySQL**,

> pager less -SFX

Or you may also try:

> SELECT * FROM sometable \G

 

Above commands will show you the output from start that you can navigate through using the arrow up/down keys or space (page by page). Press "**q**" to exit.
