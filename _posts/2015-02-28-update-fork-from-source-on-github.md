---
layout: post
title: "Update Fork from source on Github"

postday: 2015/02/28
posttime: 21_03
author: Mandeep
---
If you have ever forked someone's repository on Github and after some time it got updated and you want your fork to be up-to-date then follow the procedure:

  1. Open your fork on GitHub.
  2. Click on Pull Requests.
  3. Click on New Pull Request. By default, GitHub will compare the original with your fork, and there shouldn’t be anything to compare if you didn’t make any changes.
  4. Click on switching the base (if no changes were made in the fork) or click Edit and switch the base manually. Now GitHub will compare your fork with the original, and you should see all the latest changes.
  5. Check and select the branches carefully.
  6. Click on Create to create a pull request for this comparison and assign a predictable name to your pull request (e.g., Update from original).
  7. Click on Send pull request.
  8. Scroll down and click Merge pull request and finally Confirm merge. If your fork didn’t have any changes, you will be able to merge it automatically.

Source: http://stackoverflow.com/questions/7244321/how-to-update-github-forked-repository
