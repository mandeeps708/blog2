---
layout: post
title: "Compiling FreeCAD on Ubuntu 16.04"

postday: 2016/05/18
posttime: 23_36
author: Mandeep
---
Read my another post to compile FreeCAD on Arch: <../../../../2016/03/30/compiling-freecad-from-source-on-arch-linux/>

**Getting the source**:

git clone <https://github.com/FreeCAD/FreeCAD> free-cad-code

**Getting the dependencies**:

> sudo apt install build-essential cmake python python-matplotlib libtool libcoin80-dev libsoqt4-dev libxerces-c-dev libboost-dev libboost-filesystem-dev libboost-regex-dev libboost-program-options-dev libboost-signals-dev libboost-thread-dev libboost-python-dev libqt4-dev libqt4-opengl-dev qt4-dev-tools python-dev python-pyside pyside-tools oce-draw libeigen3-dev libqtwebkit-dev libshiboken-dev libpyside-dev libode-dev swig libzipios++-dev libfreetype6 libfreetype6-dev liboce-foundation-dev liboce-modeling-dev liboce-ocaf-dev liboce-visualization-dev liboce-ocaf-lite-dev libsimage-dev checkinstall python-pivy python-qt4 doxygen libspnav-dev

**Compiling:**

Now first we'll create a separate directory for storing our build files. This will be out-of-source build. Which means the source code directory will not get modified and there won't be any issue with git.

> mkdir build
> 
> cd build
> 
> cmake

For example, if you have two directories: build and free-cad-code, then go to build directory and run:

> cmake ../free-cad-code

Here_ free-cad-code_ is my source code directory.

While running cmake, I got an [error](https://transfer.sh/UoxOw/freecad.txt):

> make[2]: *** No rule to make target '/usr/lib/libfreeimage.so', needed by 'lib/libSMDS.so'. Stop.

This was solved by creating a symlink from /usr/lib/x86_64-linux-gnu/libfreeimage.so to /usr/lib/libfreeimage.so

> sudo ln -s /usr/lib/x86_64-linux-gnu/libfreeimage.so /usr/lib/libfreeimage.so

If the above cmake process worked successfully then proceed to the following or just analyze the error and install the missing dependencies. Now type:

> make

to start the compilation.

To speed up the compilation, I did: **make -j4** instead of **make**. It will make the full utilization of CPU and memory. Here 4 is the number of cores. You may find yours using command: **nproc **or simply run **make -j$(nproc)**

But wait, I got into a problem due to that. The C++ compiler ran out of memory while compilation. It threw an [error](https://transfer.sh/KuTa6/freecad-cpp.txt):

> c++: internal compiler error: Killed (program cc1plus)
> 
> Please submit a full bug report, with preprocessed source if appropriate.
> 
> See <file:///usr/share/doc/gcc-5/README.Bugs> for instructions.

It was due to the full memory utilization and compiler ran out of memory. I got help regarding this by one of the mentors (ickby) on _#freecad_ irc channel.

Then I tried with **make -j2** and it worked flawlessly.

**Execution:**

Execute using the following command:

> _./bin/FreeCAD_

**Source: **<http://www.freecadweb.org/wiki/?title=CompileOnUnix>
