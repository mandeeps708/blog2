---
layout: post
title: "17th June"

postday: 2014/06/17
posttime: 22_28
author: Mandeep
---
Today there were two presentations by Deepak Kumar and Kamalpreet kaur. First presentation was on Latex. Latex is basically a software used to create professional looking text documents. In this, we first have to define the layout that our document will be using. The type of output it produces is hardly to produce even by any paid software like Microsoft Word. It has many great advantages. It creates documents with stunning looks.   
     For example, While publishing a book we define chapters. At the end of the book, we need not to make the index page that shows the contents. Latex will automatically do it for you. It will include page numbers according to the chapters and subtopics. Even if we delete some of the pages then in the index the page location will automatically change accordingly.   
The second presentation was on Sage. Sage is a mathematical software used to solve mathematical equations. So it's quite good software. It also makes the graph of the equation. It works as command line as well as browser web application.

In the evening, there was also a presentation about AutoCAD. AutoCAD is a designing software. The presentation was about paper layout.
