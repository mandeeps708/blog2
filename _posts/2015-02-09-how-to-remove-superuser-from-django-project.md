---
layout: post
title: "How to remove superuser from Django Project"

postday: 2015/02/09
posttime: 23_02
author: Mandeep
---

    Open Terminal(Ctrl+Alt+T).
    Browse to your project source directory(where 'manage.py' exists):
    
    
    
    
    
    _$ cd <del>project</del>_
    
    
    
    
    
    Note: Replace 'project' with your directory name.
    
    
    
    
    
    Now execute these commands:
    $ _python manage.py shell_
    
    
    
    
    
    Then the interactive python shell with django powers will appear. Then type in:
    $ _from django.contrib.auth.models import User_
    $ _User.objects.get(username="<del>NameHere</del>", is_superuser=True).delete__()_
    
    
    
    
    
    You should replace the name of the superuser you created with NameHere in the last command.
    
    
    
    
